/*
 * This file is part of the GROMACS molecular simulation package.
 *
 * Copyright (c) 1991-2000, University of Groningen, The Netherlands.
 * Copyright (c) 2001-2004, The GROMACS development team.
 * Copyright (c) 2013,2014,2015,2016,2018 by the GROMACS development team.
 * Copyright (c) 2019,2020, by the GROMACS development team, led by
 * Mark Abraham, David van der Spoel, Berk Hess, and Erik Lindahl,
 * and including many others, as listed in the AUTHORS file in the
 * top-level source directory and at http://www.gromacs.org.
 *
 * GROMACS is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public License
 * as published by the Free Software Foundation; either version 2.1
 * of the License, or (at your option) any later version.
 *
 * GROMACS is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with GROMACS; if not, see
 * http://www.gnu.org/licenses, or write to the Free Software Foundation,
 * Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA.
 *
 * If you want to redistribute modifications to GROMACS, please
 * consider that scientific software is very special. Version
 * control is crucial - bugs must be traceable. We will be happy to
 * consider code for inclusion in the official distribution, but
 * derived work must not be called official GROMACS. Details are found
 * in the README & COPYING files - if they are missing, get the
 * official version at http://www.gromacs.org.
 *
 * To help us fund GROMACS development, we humbly ask that you cite
 * the research papers on the package. Check out http://www.gromacs.org.
 */

/*! \internal \file
 *
 *
 * \brief
 * This file contains datatypes and function declarations for internal
   use in the pull code.
 *
 * \author Berk Hess
 */

#ifndef GMX_PULLING_PULL_INTERNAL_H
#define GMX_PULLING_PULL_INTERNAL_H

#include "config.h"

#include <memory>
#include <vector>

#include "gromacs/domdec/localatomset.h"
#include "gromacs/mdtypes/pull_params.h"
#include "gromacs/utility/gmxmpi.h"

/*! \brief Determines up to what local atom count a pull group gets processed single-threaded.
 *
 * We set this limit to 1 with debug to catch bugs.
 * On Haswell with GCC 5 the cross-over point is around 400 atoms,
 * independent of thread count and hyper-threading.
 */
#ifdef NDEBUG
static const int c_pullMaxNumLocalAtomsSingleThreaded = 100;
#else
static const int c_pullMaxNumLocalAtomsSingleThreaded = 1;
#endif

class PullHistory;
enum class PbcType : int;

enum
{
    epgrppbcNONE,
    epgrppbcREFAT,
    epgrppbcCOS,
    epgrppbcPREVSTEPCOM
};

/*! \internal
 * \brief Pull group data used during pulling
 */
struct pull_group_work_t
{
    /*! \brief Constructor
     *
     * \param[in] params                  The group parameters set by the user
     * \param[in] atomSet                 The global to local atom set manager
     * \param[in] setPbcRefToPrevStepCOM Does this pull group use the COM from the previous step as reference position?
     */
    pull_group_work_t(const t_pull_group& params, gmx::LocalAtomSet atomSet, bool setPbcRefToPrevStepCOM);

    /* Data only modified at initialization */
    const t_pull_group params;   /**< The pull group parameters */
    const int          epgrppbc; /**< The type of pbc for this pull group, see enum above */
    bool               needToCalcCom; /**< Do we need to calculate the COM? (Not for group 0 or if only used as cylinder group) */
    std::vector<real>  globalWeights; /**< Weights per atom set by the user and/or mass/friction coefficients, if empty all weights are equal */

    /* Data modified only at init or at domain decomposition */
    gmx::LocalAtomSet                  atomSet;      /**< Global to local atom set mapper */
    std::vector<real>                  localWeights; /**< Weights for the local atoms */
    std::unique_ptr<gmx::LocalAtomSet> pbcAtomSet;   /**< Keeps index of the pbc reference atom.
                                                          The stored LocalAtomSet consists of exactly   one atom when pbc reference atom is required.
                                                          When no pbc refence atom is used, this   pointer   shall be null. */

    /* Data, potentially, changed at every pull call */
    real mwscale; /**< mass*weight scaling factor 1/sum w m */
    real wscale;  /**< scaling factor for the weights: sum w m/sum w w m */
    real invtm;   /**< inverse total mass of the group: 1/wscale sum w m */
    std::vector<gmx::BasicVector<double>> mdw; /**< mass*gradient(weight) for atoms */
    std::vector<double>                   dv;  /**< distance to the other group(s) along vec */
    dvec                                  x;   /**< COM before update */
    dvec                                  xp;  /**< COM after update before constraining */
    dvec                                  x_prev_step; /**< center of mass of the previous step */
};

/* Struct describing the instantaneous spatial layout of a pull coordinate */
struct PullCoordSpatialData
{
    dvec   dr01;       /* The direction vector of group 1 relative to group 0 */
    dvec   dr23;       /* The direction vector of group 3 relative to group 2 */
    dvec   dr45;       /* The direction vector of group 5 relative to group 4 */
    dvec   vec;        /* The pull direction */
    double vec_len;    /* Length of vec for direction-relative */
    dvec   ffrad;      /* conversion factor from vec to radial force */
    double cyl_dev;    /* The deviation from the reference position */
    dvec   planevec_m; /* Normal of plane for groups 0, 1, 2, 3 for geometry dihedral */
    dvec   planevec_n; /* Normal of plane for groups 2, 3, 4, 5 for geometry dihedral */

    double value; /* The current value of the coordinate, units of nm or rad */
};

/* Struct with parameters and force evaluation local data for a pull coordinate */
struct pull_coord_work_t
{
    /* Constructor */
    pull_coord_work_t(const t_pull_coord& params) :
        params(params),
        value_ref(0),
        spatialData(),
        scalarForce(0),
        bExternalPotentialProviderHasBeenRegistered(false)
    {
    }

    const t_pull_coord params; /* Pull coordinate parameters */

    double value_ref; /* The reference value, usually init+rate*t, units of nm or rad */

    PullCoordSpatialData spatialData; /* Data defining the current geometry */

    double scalarForce; /* Scalar force for this coordinate */

    /* For external-potential coordinates only, for checking if a provider has been registered */
    bool bExternalPotentialProviderHasBeenRegistered;

    /* 
     * Variables for slice pulling, see:
     * Hub and Awasthi
     * Probing a continuous polar defect: A reaction coordinate for pore formation in lipid membranes 
     * J. Chem. Theory Comput., 13, 2352-2366 (2017)
     */
    double  xiChain;             /* Current position along the chain reaction coordinate (Hub/Awasthi, 2017) */
    dvec*   gradient_xiChain;    /* Gradient of xi wrt. atomic coordaintes, index of pull group atoms in local thread */
    int     nalloc_grad_xiChain; /* Allocation size for gradient_xi and rRelCyl */
    double* sum_fz;              /* F[s] = sum_i fz(z_i, z_s) */
    double* wcyl;                /* wcyl[s] = tanh( sum_i fz), used as weight for computing the average COM over all slices */
    double* Np;                  /* N_s^(p) = sum_i fz*frad, number of polar atoms wihtin cylinder in this slice */
    dvec*   rRelCyl;             /* Array of size nat (not nat_loc) of coordinates relative to cylinder center */
    double* Ax;                  /* Eq. 20 */
    double* Ay;
    dvec    Ftotal;              /* Sum of forces acting on polar atoms, used to add counter forces on lipid tail atoms */

    dvec* comPBCSlice_cos; /* for computing the COM in the periodic system <sin(2pi*xi/Lx) > */
    dvec* comPBCSlice_sin;

    dvec* comPBCSlice;
    dvec  comPBCAll; /* Periodic COM over all slices */
    dvec  comPBCAll_sin;
    dvec  comPBCAll_cos;
    dvec  comPBCAll_oneOver_s2c2;

    /*
     *  Variables for pore expansion (after opening the pore with slice slicepulling)
     */
    double xiPoreExpand;             /* The overall reaction coordinate */
    dvec*  gradient_xiPoreExpand;    /* Gradient of xiExpandPore wrt. atomic coords */
    int    nalloc_grad_xiPoreExpand; /* Allocation size for grad_xiPoreExpand */
    double nPolar;                   /* Nr of polar atoms (of pull group 2) inslide layer */
    double poreR;                    /* radius of the pore */
};

/* Struct for storing vectorial forces for a pull coordinate */
struct PullCoordVectorForces
{
    dvec force01; /* Force due to the pulling/constraining for groups 0, 1 */
    dvec force23; /* Force for groups 2 and 3 */
    dvec force45; /* Force for groups 4 and 5 */
};

/* Struct for sums over (local) atoms in a pull group */
struct ComSums
{
    /* For normal weighting */
    double sum_wm;   /* Sum of weight*mass        */
    double sum_wwm;  /* Sum of weight*weight*mass */
    dvec   sum_wmx;  /* Sum of weight*mass*x      */
    dvec   sum_wmxp; /* Sum of weight*mass*xp     */

    /* For cosine weighting */
    double sum_cm;  /* Sum of cos(x)*mass          */
    double sum_sm;  /* Sum of sin(x)*mass          */
    double sum_ccm; /* Sum of cos(x)*cos(x)*mass   */
    double sum_csm; /* Sum of cos(x)*sin(x)*mass   */
    double sum_ssm; /* Sum of sin(x)*sin(x)*mass   */
    double sum_cmp; /* Sum of cos(xp)*sin(xp)*mass */
    double sum_smp; /* Sum of sin(xp)*sin(xp)*mass */

    /* Dummy data to ensure adjacent elements in an array are separated
     * by a cache line size, max 128 bytes.
     * TODO: Replace this by some automated mechanism.
     */
    int dummy[32];
};

/*! \brief The normal COM buffer needs 3 elements per group */
static constexpr int c_comBufferStride = 3;

/*! \brief The cylinder buffer needs 9 elements per group */
static constexpr int c_cylinderBufferStride = 9;

struct pull_comm_t
{
    gmx_bool bParticipateAll; /* Do all ranks always participate in pulling? */
    gmx_bool bParticipate;    /* Does our rank participate in pulling? */
#if GMX_MPI
    MPI_Comm mpi_comm_com; /* Communicator for pulling */
#endif
    int  nparticipate; /* The number of ranks participating */
    bool isMasterRank; /* Tells whether our rank is the master rank and thus should add the pull virial */

    int64_t setup_count; /* The number of decomposition calls */
    int64_t must_count;  /* The last count our rank needed to be part */

    /* Buffers for parallel reductions */
    std::vector<gmx::RVec>                pbcAtomBuffer; /* COM calculation buffer */
    std::vector<gmx::BasicVector<double>> comBuffer;     /* COM calculation buffer */
    std::vector<double> cylinderBuffer; /* cylinder ref. groups calculation buffer */
};

// The COM pull force calculation data structure
// TODO Convert this into a ForceProvider
struct pull_t
{
    /* Global parameters */
    pull_params_t params; /* The pull parameters, from inputrec */

    gmx_bool bPotential;  /* Are there coordinates with potential? */
    gmx_bool bConstraint; /* Are there constrained coordinates? */
    gmx_bool bAngle;      /* Are there angle geometry coordinates? */
    gmx_bool bSlice;      /* Are there any chain coordinate (slice pulling) group groups */
    gmx_bool bPoreExpand; /* Are there any pore expansion */

    PbcType  pbcType;   /* the boundary conditions */
    int      npbcdim;   /* do pbc in dims 0 <= dim < npbcdim */
    gmx_bool bRefAt;    /* do we need reference atoms for a group COM ? */
    int      cosdim;    /* dimension for cosine weighting, -1 if none */
    gmx_bool bCylinder; /* Is group 0 a cylinder group? */

    /* Parameters + dynamic data for groups */
    std::vector<pull_group_work_t> group; /* The pull group param and work data */
    std::vector<pull_group_work_t> dyna;  /* Dynamic groups for geom=cylinder */

    /* Parameters + dynamic data for coordinates */
    std::vector<pull_coord_work_t> coord; /* The pull group param and work data */

    /* Global dynamic data */
    gmx_bool bSetPBCatoms; /* Do we need to set x_pbc for the groups? */

    int                  nthreads; /* Number of threads used by the pull code */
    std::vector<ComSums> comSums;  /* Work array for summing for COM, 1 entry per thread */

    pull_comm_t comm; /* Communication parameters, communicator and buffers */

    FILE* out_x; /* Output file for pull data */
    FILE* out_f; /* Output file for pull data */

    bool bXOutAverage; /* Output average pull coordinates */
    bool bFOutAverage; /* Output average pull forces */

    PullHistory* coordForceHistory; /* Pull coordinate and force history */

    /* The number of coordinates using an external potential */
    int numCoordinatesWithExternalPotential;
    /* Counter for checking external potential registration */
    int numUnregisteredExternalPotentials;
    /* */
    int numExternalPotentialsStillToBeAppliedThisStep;

    /*
     * Global parameters for slice pulling
     */
    double slice_d;     /* pulling water in to slices: slice thickness, switch width, number of slices */
    double slice_h;     /* Width h of switch func */
    double slice_gamma; /* Parameter gamma, determines how many atoms are needed in a slice to fill the slice
                           to a fraction zeta, see next option. In the original paper (Hub/Awasthi, JCTC 2017),
                           we did not have a parameter gamma so we used gamma=1. */
    double slice_zeta;  /* Parameter zeta, determines to which degree the slice is fille by the first gamma atoms. */
    double slice_cyl_r; /* Radius of the cylinder */
    double slice_cyl_w; /* Switch with of the cylinder */
    int    nslices;     /* Number of slices. Use nslices and slice_d such that all slices span the entire membrane
                           but such that the slices only slightly penetrate into the head group region */
    gmx_bool bSliceApplyCounterForce;     /* Apply Counter forces on ref group during slice pulling  */
    double comPBCAll_oneOver_s2c2_cutoff; /* Maximum value allowed for comPBCAll_oneOver_s2c2, avoids
                                             numerical instabilities when we don't have any pore. Has no effect
                                             when a pore is only slightly open. */
    gmx_bool bSliceFreezeCylinder;  /* Freeze the cylinder at 0/0, could be useful for small xi, where the cylider X/Y is
                                       poorly defined and may rapidly fluctuate in the xy plane */
    double   sliceFreezeCylinderXiRef; /* Freeze cylinder only if refernce pull position is below this value. 
                                          Useful to avoid that the cylinder moves rapidly in x-y plane at low xi, where
                                          a partial defect has not yet formed. */
    double   sliceFreezeCylinder_x; /* Freeze cylinder to this xy position */
    double   sliceFreezeCylinder_y;
    gmx_bool bSliceApplyCylForce;   /* Apply forces due to derivative of the cylinder position wrt. to the atomic
                                       coordinates. Only for testing. */
    double sliceSetCylinder_z;      /* Z position of cylinder, if not (as normally done) defined as the center of mass of 
                                       pul group 1 */
    int sliceNstOutput;             /* Frequency of writing slice pull info to stdout */

    /*
     * Global parameters for expanding a pore
     */
    double poreExpansion_H;    /* Height of the layer. Should be restricted to the hydrophobic core of the membrane. */
    double poreExpansion_Rmax; /* Only atoms within a large cylinder of radius Rmax contribute to the layer and, hence,
                                  to the radius of the pore. */
    double poreExpansion_RmaxRelative;
                               /* Same as Rmax, but now fix Rmax to the a value larger by XXX nm relative to the current
                                  pull refrence (e.g. 1nm). In case of heavy membrane undulations, e.g. with a very thin membrane
                                  or with a membrane with DMSO, using a cylinder with a fixed Rmax can lead to massive problems
                                  and integration errors */
    double poreExpansion_w;    /* Width of the switch region of the layer, typically 0.05nm */
    double poreExpansion_v0;   /* Approximate volume per polar atom, used to compute the radius from the number of polar atoms nW:
                                  R = sqrt( nW * v0 / pi*H ). Example: v0 = 0.02996 nm3 for water. */
    double xiChainSwitch;      /* xiChain value, where we switch from the chain coordinate to the pore expansion coordinate */
    double xiChainSwitchDelta; /* Width along xiChain, where we switch from the chain coordinate to the pore expansion coordinate */
    double poreExpansion_R0;   /* Pore radius, when xiChain ~= xiChainSwitch */
    double frozenSwitch;       /* Value, to which the switch function is frozen, if requested by PORE_EXPANSION_FROZEN_SWITCH */
};

/*! \brief Copies the pull group COM of the previous step from the checkpoint state to the pull state
 *
 * \param[in]   pull  The COM pull force calculation data structure
 * \param[in]   state The global state container
 */
void setPrevStepPullComFromState(struct pull_t* pull, const t_state* state);

/*! \brief Resizes the vector, in the state container, containing the COMs from the previous step
 *
 * \param[in]   state The global state container
 * \param[in]   pull  The COM pull force calculation data structure
 */
void allocStatePrevStepPullCom(t_state* state, const pull_t* pull);


#endif
