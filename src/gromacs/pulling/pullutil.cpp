/*
 * This file is part of the GROMACS molecular simulation package.
 *
 * Copyright (c) 1991-2000, University of Groningen, The Netherlands.
 * Copyright (c) 2001-2004, The GROMACS development team.
 * Copyright (c) 2013,2014,2015,2016,2017 by the GROMACS development team.
 * Copyright (c) 2018,2019,2020, by the GROMACS development team, led by
 * Mark Abraham, David van der Spoel, Berk Hess, and Erik Lindahl,
 * and including many others, as listed in the AUTHORS file in the
 * top-level source directory and at http://www.gromacs.org.
 *
 * GROMACS is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public License
 * as published by the Free Software Foundation; either version 2.1
 * of the License, or (at your option) any later version.
 *
 * GROMACS is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with GROMACS; if not, see
 * http://www.gnu.org/licenses, or write to the Free Software Foundation,
 * Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA.
 *
 * If you want to redistribute modifications to GROMACS, please
 * consider that scientific software is very special. Version
 * control is crucial - bugs must be traceable. We will be happy to
 * consider code for inclusion in the official distribution, but
 * derived work must not be called official GROMACS. Details are found
 * in the README & COPYING files - if they are missing, get the
 * official version at http://www.gromacs.org.
 *
 * To help us fund GROMACS development, we humbly ask that you cite
 * the research papers on the package. Check out http://www.gromacs.org.
 */
#include "gmxpre.h"

#include "config.h"

#include <cassert>
#include <cstdlib>

#include "gromacs/fileio/confio.h"
#include "gromacs/gmxlib/network.h"
#include "gromacs/math/functions.h"
#include "gromacs/math/utilities.h"
#include "gromacs/math/vec.h"
#include "gromacs/mdtypes/commrec.h"
#include "gromacs/mdtypes/inputrec.h"
#include "gromacs/mdtypes/md_enums.h"
#include "gromacs/mdtypes/mdatom.h"
#include "gromacs/mdtypes/state.h"
#include "gromacs/pbcutil/pbc.h"
#include "gromacs/pulling/pull.h"
#include "gromacs/utility/fatalerror.h"
#include "gromacs/utility/futil.h"
#include "gromacs/utility/gmxassert.h"
#include "gromacs/utility/real.h"
#include "gromacs/utility/smalloc.h"

#include "pull_internal.h"

#if GMX_MPI

// Helper function to deduce MPI datatype from the type of data
gmx_unused static MPI_Datatype mpiDatatype(const float gmx_unused* data)
{
    return MPI_FLOAT;
}

// Helper function to deduce MPI datatype from the type of data
gmx_unused static MPI_Datatype mpiDatatype(const double gmx_unused* data)
{
    return MPI_DOUBLE;
}

#endif // GMX_MPI

#if !GMX_DOUBLE
// Helper function; note that gmx_sum(d) should actually be templated
gmx_unused static void gmxAllReduce(int n, real* data, const t_commrec* cr)
{
    gmx_sum(n, data, cr);
}
#endif

// Helper function; note that gmx_sum(d) should actually be templated
gmx_unused static void gmxAllReduce(int n, double* data, const t_commrec* cr)
{
    gmx_sumd(n, data, cr);
}

// Reduce data of n elements over all ranks currently participating in pull
template<typename T>
static void pullAllReduce(const t_commrec* cr, pull_comm_t* comm, int n, T* data)
{
    if (cr != nullptr && PAR(cr))
    {
        if (comm->bParticipateAll)
        {
            /* Sum the contributions over all DD ranks */
            gmxAllReduce(n, data, cr);
        }
        else
        {
            /* Separate branch because gmx_sum uses cr->mpi_comm_mygroup */
#if GMX_MPI
#    if MPI_IN_PLACE_EXISTS
            MPI_Allreduce(MPI_IN_PLACE, data, n, mpiDatatype(data), MPI_SUM, comm->mpi_comm_com);
#    else
            std::vector<T> buf(n);

            MPI_Allreduce(data, buf.data(), n, mpiDatatype(data), MPI_SUM, comm->mpi_comm_com);

            /* Copy the result from the buffer to the input/output data */
            for (int i = 0; i < n; i++)
            {
                data[i] = buf[i];
            }
#    endif
#else
            gmx_incons("comm->bParticipateAll=FALSE without GMX_MPI");
#endif
        }
    }
}

/* Copies the coordinates of the PBC atom of pgrp to x_pbc.
 * When those coordinates are not available on this rank, clears x_pbc.
 */
static void setPbcAtomCoords(const pull_group_work_t& pgrp, const rvec* x, rvec x_pbc)
{
    if (pgrp.pbcAtomSet != nullptr)
    {
        if (pgrp.pbcAtomSet->numAtomsLocal() > 0)
        {
            /* We have the atom locally, copy its coordinates */
            copy_rvec(x[pgrp.pbcAtomSet->localIndex()[0]], x_pbc);
        }
        else
        {
            /* Another rank has it, clear the coordinates for MPI_Allreduce */
            clear_rvec(x_pbc);
        }
    }
    else
    {
        copy_rvec(x[pgrp.params.pbcatom], x_pbc);
    }
}

static void pull_set_pbcatoms(const t_commrec* cr, struct pull_t* pull, const rvec* x, gmx::ArrayRef<gmx::RVec> x_pbc)
{
    int numPbcAtoms = 0;
    for (size_t g = 0; g < pull->group.size(); g++)
    {
        const pull_group_work_t& group = pull->group[g];
        if (group.needToCalcCom && (group.epgrppbc == epgrppbcREFAT || group.epgrppbc == epgrppbcPREVSTEPCOM))
        {
            setPbcAtomCoords(pull->group[g], x, x_pbc[g]);
            numPbcAtoms++;
        }
        else
        {
            clear_rvec(x_pbc[g]);
        }
    }

    if (cr && PAR(cr) && numPbcAtoms > 0)
    {
        /* Sum over participating ranks to get x_pbc from the home ranks.
         * This can be very expensive at high parallelization, so we only
         * do this after each DD repartitioning.
         */
        pullAllReduce(cr, &pull->comm, pull->group.size() * DIM, static_cast<real*>(x_pbc[0]));
    }
}

static void
make_cyl_refgrps(const t_commrec* cr, pull_t* pull, const real* masses, t_pbc* pbc, double t, const rvec* x)
{
    pull_comm_t* comm = &pull->comm;

    GMX_ASSERT(comm->cylinderBuffer.size() == pull->coord.size() * c_cylinderBufferStride,
               "cylinderBuffer should have the correct size");

    double inv_cyl_r2 = 1.0 / gmx::square(pull->params.cylinder_r);

    /* loop over all groups to make a reference group for each*/
    for (size_t c = 0; c < pull->coord.size(); c++)
    {
        pull_coord_work_t* pcrd;
        double             sum_a, wmass, wwmass;
        dvec               radf_fac0, radf_fac1;

        pcrd = &pull->coord[c];

        sum_a  = 0;
        wmass  = 0;
        wwmass = 0;
        clear_dvec(radf_fac0);
        clear_dvec(radf_fac1);

        if (pcrd->params.eGeom == epullgCYL)
        {
            /* pref will be the same group for all pull coordinates */
            const pull_group_work_t& pref  = pull->group[pcrd->params.group[0]];
            const pull_group_work_t& pgrp  = pull->group[pcrd->params.group[1]];
            pull_group_work_t&       pdyna = pull->dyna[c];
            rvec                     direction;
            copy_dvec_to_rvec(pcrd->spatialData.vec, direction);

            /* Since we have not calculated the COM of the cylinder group yet,
             * we calculate distances with respect to location of the pull
             * group minus the reference position along the vector.
             * here we already have the COM of the pull group. This resolves
             * any PBC issues and we don't need to use a PBC-atom here.
             */
            if (pcrd->params.rate != 0)
            {
                /* With rate=0, value_ref is set initially */
                pcrd->value_ref = pcrd->params.init + pcrd->params.rate * t;
            }
            rvec reference;
            for (int m = 0; m < DIM; m++)
            {
                reference[m] = pgrp.x[m] - pcrd->spatialData.vec[m] * pcrd->value_ref;
            }

            auto localAtomIndices = pref.atomSet.localIndex();

            /* This actually only needs to be done at init or DD time,
             * but resizing with the same size does not cause much overhead.
             */
            pdyna.localWeights.resize(localAtomIndices.size());
            pdyna.mdw.resize(localAtomIndices.size());
            pdyna.dv.resize(localAtomIndices.size());

            /* loop over all atoms in the main ref group */
            for (gmx::index indexInSet = 0; indexInSet < localAtomIndices.ssize(); indexInSet++)
            {
                int  atomIndex = localAtomIndices[indexInSet];
                rvec dx;
                pbc_dx_aiuc(pbc, x[atomIndex], reference, dx);
                double axialLocation = iprod(direction, dx);
                dvec   radialLocation;
                double dr2 = 0;
                for (int m = 0; m < DIM; m++)
                {
                    /* Determine the radial components */
                    radialLocation[m] = dx[m] - axialLocation * direction[m];
                    dr2 += gmx::square(radialLocation[m]);
                }
                double dr2_rel = dr2 * inv_cyl_r2;

                if (dr2_rel < 1)
                {
                    /* add atom to sum of COM and to weight array */

                    double mass = masses[atomIndex];
                    /* The radial weight function is 1-2x^2+x^4,
                     * where x=r/cylinder_r. Since this function depends
                     * on the radial component, we also get radial forces
                     * on both groups.
                     */
                    double weight                  = 1 + (-2 + dr2_rel) * dr2_rel;
                    double dweight_r               = (-4 + 4 * dr2_rel) * inv_cyl_r2;
                    pdyna.localWeights[indexInSet] = weight;
                    sum_a += mass * weight * axialLocation;
                    wmass += mass * weight;
                    wwmass += mass * weight * weight;
                    dvec mdw;
                    dsvmul(mass * dweight_r, radialLocation, mdw);
                    copy_dvec(mdw, pdyna.mdw[indexInSet]);
                    /* Currently we only have the axial component of the
                     * offset from the cylinder COM up to an unkown offset.
                     * We add this offset after the reduction needed
                     * for determining the COM of the cylinder group.
                     */
                    pdyna.dv[indexInSet] = axialLocation;
                    for (int m = 0; m < DIM; m++)
                    {
                        radf_fac0[m] += mdw[m];
                        radf_fac1[m] += mdw[m] * axialLocation;
                    }
                }
                else
                {
                    pdyna.localWeights[indexInSet] = 0;
                }
            }
        }

        auto buffer = gmx::arrayRefFromArray(
                comm->cylinderBuffer.data() + c * c_cylinderBufferStride, c_cylinderBufferStride);

        buffer[0] = wmass;
        buffer[1] = wwmass;
        buffer[2] = sum_a;

        buffer[3] = radf_fac0[XX];
        buffer[4] = radf_fac0[YY];
        buffer[5] = radf_fac0[ZZ];

        buffer[6] = radf_fac1[XX];
        buffer[7] = radf_fac1[YY];
        buffer[8] = radf_fac1[ZZ];
    }

    if (cr != nullptr && PAR(cr))
    {
        /* Sum the contributions over the ranks */
        pullAllReduce(cr, comm, pull->coord.size() * c_cylinderBufferStride, comm->cylinderBuffer.data());
    }

    for (size_t c = 0; c < pull->coord.size(); c++)
    {
        pull_coord_work_t* pcrd;

        pcrd = &pull->coord[c];

        if (pcrd->params.eGeom == epullgCYL)
        {
            pull_group_work_t*    pdyna       = &pull->dyna[c];
            pull_group_work_t*    pgrp        = &pull->group[pcrd->params.group[1]];
            PullCoordSpatialData& spatialData = pcrd->spatialData;

            auto buffer = gmx::constArrayRefFromArray(
                    comm->cylinderBuffer.data() + c * c_cylinderBufferStride, c_cylinderBufferStride);
            double wmass   = buffer[0];
            double wwmass  = buffer[1];
            pdyna->mwscale = 1.0 / wmass;
            /* Cylinder pulling can't be used with constraints, but we set
             * wscale and invtm anyhow, in case someone would like to use them.
             */
            pdyna->wscale = wmass / wwmass;
            pdyna->invtm  = wwmass / (wmass * wmass);

            /* We store the deviation of the COM from the reference location
             * used above, since we need it when we apply the radial forces
             * to the atoms in the cylinder group.
             */
            spatialData.cyl_dev = 0;
            for (int m = 0; m < DIM; m++)
            {
                double reference = pgrp->x[m] - spatialData.vec[m] * pcrd->value_ref;
                double dist      = -spatialData.vec[m] * buffer[2] * pdyna->mwscale;
                pdyna->x[m]      = reference - dist;
                spatialData.cyl_dev += dist;
            }
            /* Now we know the exact COM of the cylinder reference group,
             * we can determine the radial force factor (ffrad) that when
             * multiplied with the axial pull force will give the radial
             * force on the pulled (non-cylinder) group.
             */
            for (int m = 0; m < DIM; m++)
            {
                spatialData.ffrad[m] = (buffer[6 + m] + buffer[3 + m] * spatialData.cyl_dev) / wmass;
            }

            if (debug)
            {
                fprintf(debug, "Pull cylinder group %zu:%8.3f%8.3f%8.3f m:%8.3f\n", c, pdyna->x[0],
                        pdyna->x[1], pdyna->x[2], 1.0 / pdyna->invtm);
                fprintf(debug, "ffrad %8.3f %8.3f %8.3f\n", spatialData.ffrad[XX],
                        spatialData.ffrad[YY], spatialData.ffrad[ZZ]);
            }
        }
    }
}

static double atan2_0_2pi(double y, double x)
{
    double a;

    a = atan2(y, x);
    if (a < 0)
    {
        a += 2.0 * M_PI;
    }
    return a;
}

static void sum_com_part(const pull_group_work_t* pgrp,
                         int                      ind_start,
                         int                      ind_end,
                         const rvec*              x,
                         const rvec*              xp,
                         const real*              mass,
                         const t_pbc*             pbc,
                         const rvec               x_pbc,
                         ComSums*                 sum_com)
{
    double sum_wm   = 0;
    double sum_wwm  = 0;
    dvec   sum_wmx  = { 0, 0, 0 };
    dvec   sum_wmxp = { 0, 0, 0 };

    auto localAtomIndices = pgrp->atomSet.localIndex();
    for (int i = ind_start; i < ind_end; i++)
    {
        int  ii = localAtomIndices[i];
        real wm;
        if (pgrp->localWeights.empty())
        {
            wm = mass[ii];
            sum_wm += wm;
        }
        else
        {
            real w;

            w  = pgrp->localWeights[i];
            wm = w * mass[ii];
            sum_wm += wm;
            sum_wwm += wm * w;
        }
        if (pgrp->epgrppbc == epgrppbcNONE)
        {
            /* Plain COM: sum the coordinates */
            for (int d = 0; d < DIM; d++)
            {
                sum_wmx[d] += wm * x[ii][d];
            }
            if (xp)
            {
                for (int d = 0; d < DIM; d++)
                {
                    sum_wmxp[d] += wm * xp[ii][d];
                }
            }
        }
        else
        {
            rvec dx;

            /* Sum the difference with the reference atom */
            pbc_dx(pbc, x[ii], x_pbc, dx);
            for (int d = 0; d < DIM; d++)
            {
                sum_wmx[d] += wm * dx[d];
            }
            if (xp)
            {
                /* For xp add the difference between xp and x to dx,
                 * such that we use the same periodic image,
                 * also when xp has a large displacement.
                 */
                for (int d = 0; d < DIM; d++)
                {
                    sum_wmxp[d] += wm * (dx[d] + xp[ii][d] - x[ii][d]);
                }
            }
        }
    }

    sum_com->sum_wm  = sum_wm;
    sum_com->sum_wwm = sum_wwm;
    copy_dvec(sum_wmx, sum_com->sum_wmx);
    if (xp)
    {
        copy_dvec(sum_wmxp, sum_com->sum_wmxp);
    }
}

static void sum_com_part_cosweight(const pull_group_work_t* pgrp,
                                   int                      ind_start,
                                   int                      ind_end,
                                   int                      cosdim,
                                   real                     twopi_box,
                                   const rvec*              x,
                                   const rvec*              xp,
                                   const real*              mass,
                                   ComSums*                 sum_com)
{
    /* Cosine weighting geometry */
    double sum_cm  = 0;
    double sum_sm  = 0;
    double sum_ccm = 0;
    double sum_csm = 0;
    double sum_ssm = 0;
    double sum_cmp = 0;
    double sum_smp = 0;

    auto localAtomIndices = pgrp->atomSet.localIndex();

    for (int i = ind_start; i < ind_end; i++)
    {
        int  ii = localAtomIndices[i];
        real m  = mass[ii];
        /* Determine cos and sin sums */
        real cw = std::cos(x[ii][cosdim] * twopi_box);
        real sw = std::sin(x[ii][cosdim] * twopi_box);
        sum_cm += static_cast<double>(cw * m);
        sum_sm += static_cast<double>(sw * m);
        sum_ccm += static_cast<double>(cw * cw * m);
        sum_csm += static_cast<double>(cw * sw * m);
        sum_ssm += static_cast<double>(sw * sw * m);

        if (xp != nullptr)
        {
            real cw = std::cos(xp[ii][cosdim] * twopi_box);
            real sw = std::sin(xp[ii][cosdim] * twopi_box);
            sum_cmp += static_cast<double>(cw * m);
            sum_smp += static_cast<double>(sw * m);
        }
    }

    sum_com->sum_cm  = sum_cm;
    sum_com->sum_sm  = sum_sm;
    sum_com->sum_ccm = sum_ccm;
    sum_com->sum_csm = sum_csm;
    sum_com->sum_ssm = sum_ssm;
    sum_com->sum_cmp = sum_cmp;
    sum_com->sum_smp = sum_smp;
}

/* calculates center of mass of selection index from all coordinates x */
// Compiler segfault with 2019_update_5 and 2020_initial
#if defined(__INTEL_COMPILER) \
        && ((__INTEL_COMPILER == 1900 && __INTEL_COMPILER_UPDATE >= 5) || __INTEL_COMPILER >= 1910)
#    pragma intel optimization_level 2
#endif
void pull_calc_coms(const t_commrec* cr, pull_t* pull, const real* masses, t_pbc* pbc, double t, const rvec x[], rvec* xp)
{
    real         twopi_box = 0;
    pull_comm_t* comm;

    comm = &pull->comm;

    GMX_ASSERT(comm->pbcAtomBuffer.size() == pull->group.size(),
               "pbcAtomBuffer should have size number of groups");
    GMX_ASSERT(comm->comBuffer.size() == pull->group.size() * c_comBufferStride,
               "comBuffer should have size #group*c_comBufferStride");

    if (pull->bRefAt && pull->bSetPBCatoms)
    {
        pull_set_pbcatoms(cr, pull, x, comm->pbcAtomBuffer);

        if (cr != nullptr && DOMAINDECOMP(cr))
        {
            /* We can keep these PBC reference coordinates fixed for nstlist
             * steps, since atoms won't jump over PBC.
             * This avoids a global reduction at the next nstlist-1 steps.
             * Note that the exact values of the pbc reference coordinates
             * are irrelevant, as long all atoms in the group are within
             * half a box distance of the reference coordinate.
             */
            pull->bSetPBCatoms = FALSE;
        }
    }

    if (pull->cosdim >= 0)
    {
        int m;

        assert(pull->npbcdim <= DIM);

        for (m = pull->cosdim + 1; m < pull->npbcdim; m++)
        {
            if (pbc->box[m][pull->cosdim] != 0)
            {
                gmx_fatal(FARGS, "Can not do cosine weighting for trilinic dimensions");
            }
        }
        twopi_box = 2.0 * M_PI / pbc->box[pull->cosdim][pull->cosdim];
    }

    for (size_t g = 0; g < pull->group.size(); g++)
    {
        pull_group_work_t* pgrp = &pull->group[g];

        /* Cosine-weighted COMs behave different from all other weighted COMs
         * in the sense that the weights depend on instantaneous coordinates,
         * not on pre-set weights. Thus we resize the local weight buffer here.
         */
        if (pgrp->epgrppbc == epgrppbcCOS)
        {
            pgrp->localWeights.resize(pgrp->atomSet.localIndex().size());
        }

        auto comBuffer = gmx::arrayRefFromArray(comm->comBuffer.data() + g * c_comBufferStride,
                                                c_comBufferStride);

        if (pgrp->needToCalcCom)
        {
            if (pgrp->epgrppbc != epgrppbcCOS)
            {
                rvec x_pbc = { 0, 0, 0 };

                switch (pgrp->epgrppbc)
                {
                    case epgrppbcREFAT:
                        /* Set the pbc atom */
                        copy_rvec(comm->pbcAtomBuffer[g], x_pbc);
                        break;
                    case epgrppbcPREVSTEPCOM:
                        /* Set the pbc reference to the COM of the group of the last step */
                        copy_dvec_to_rvec(pgrp->x_prev_step, comm->pbcAtomBuffer[g]);
                        copy_dvec_to_rvec(pgrp->x_prev_step, x_pbc);
                }

                /* The final sums should end up in comSums[0] */
                ComSums& comSumsTotal = pull->comSums[0];

                /* If we have a single-atom group the mass is irrelevant, so
                 * we can remove the mass factor to avoid division by zero.
                 * Note that with constraint pulling the mass does matter, but
                 * in that case a check group mass != 0 has been done before.
                 */
                if (pgrp->params.ind.size() == 1 && pgrp->atomSet.numAtomsLocal() == 1
                    && masses[pgrp->atomSet.localIndex()[0]] == 0)
                {
                    GMX_ASSERT(xp == nullptr,
                               "We should not have groups with zero mass with constraints, i.e. "
                               "xp!=NULL");

                    /* Copy the single atom coordinate */
                    for (int d = 0; d < DIM; d++)
                    {
                        comSumsTotal.sum_wmx[d] = x[pgrp->atomSet.localIndex()[0]][d];
                    }
                    /* Set all mass factors to 1 to get the correct COM */
                    comSumsTotal.sum_wm  = 1;
                    comSumsTotal.sum_wwm = 1;
                }
                else if (pgrp->atomSet.numAtomsLocal() <= c_pullMaxNumLocalAtomsSingleThreaded)
                {
                    sum_com_part(pgrp, 0, pgrp->atomSet.numAtomsLocal(), x, xp, masses, pbc, x_pbc,
                                 &comSumsTotal);
                }
                else
                {
#pragma omp parallel for num_threads(pull->nthreads) schedule(static)
                    for (int t = 0; t < pull->nthreads; t++)
                    {
                        int ind_start = (pgrp->atomSet.numAtomsLocal() * (t + 0)) / pull->nthreads;
                        int ind_end   = (pgrp->atomSet.numAtomsLocal() * (t + 1)) / pull->nthreads;
                        sum_com_part(pgrp, ind_start, ind_end, x, xp, masses, pbc, x_pbc,
                                     &pull->comSums[t]);
                    }

                    /* Reduce the thread contributions to sum_com[0] */
                    for (int t = 1; t < pull->nthreads; t++)
                    {
                        comSumsTotal.sum_wm += pull->comSums[t].sum_wm;
                        comSumsTotal.sum_wwm += pull->comSums[t].sum_wwm;
                        dvec_inc(comSumsTotal.sum_wmx, pull->comSums[t].sum_wmx);
                        dvec_inc(comSumsTotal.sum_wmxp, pull->comSums[t].sum_wmxp);
                    }
                }

                if (pgrp->localWeights.empty())
                {
                    comSumsTotal.sum_wwm = comSumsTotal.sum_wm;
                }

                /* Copy local sums to a buffer for global summing */
                copy_dvec(comSumsTotal.sum_wmx, comBuffer[0]);

                copy_dvec(comSumsTotal.sum_wmxp, comBuffer[1]);

                comBuffer[2][0] = comSumsTotal.sum_wm;
                comBuffer[2][1] = comSumsTotal.sum_wwm;
                comBuffer[2][2] = 0;
            }
            else
            {
                /* Cosine weighting geometry.
                 * This uses a slab of the system, thus we always have many
                 * atoms in the pull groups. Therefore, always use threads.
                 */
#pragma omp parallel for num_threads(pull->nthreads) schedule(static)
                for (int t = 0; t < pull->nthreads; t++)
                {
                    int ind_start = (pgrp->atomSet.numAtomsLocal() * (t + 0)) / pull->nthreads;
                    int ind_end   = (pgrp->atomSet.numAtomsLocal() * (t + 1)) / pull->nthreads;
                    sum_com_part_cosweight(pgrp, ind_start, ind_end, pull->cosdim, twopi_box, x, xp,
                                           masses, &pull->comSums[t]);
                }

                /* Reduce the thread contributions to comSums[0] */
                ComSums& comSumsTotal = pull->comSums[0];
                for (int t = 1; t < pull->nthreads; t++)
                {
                    comSumsTotal.sum_cm += pull->comSums[t].sum_cm;
                    comSumsTotal.sum_sm += pull->comSums[t].sum_sm;
                    comSumsTotal.sum_ccm += pull->comSums[t].sum_ccm;
                    comSumsTotal.sum_csm += pull->comSums[t].sum_csm;
                    comSumsTotal.sum_ssm += pull->comSums[t].sum_ssm;
                    comSumsTotal.sum_cmp += pull->comSums[t].sum_cmp;
                    comSumsTotal.sum_smp += pull->comSums[t].sum_smp;
                }

                /* Copy local sums to a buffer for global summing */
                comBuffer[0][0] = comSumsTotal.sum_cm;
                comBuffer[0][1] = comSumsTotal.sum_sm;
                comBuffer[0][2] = 0;
                comBuffer[1][0] = comSumsTotal.sum_ccm;
                comBuffer[1][1] = comSumsTotal.sum_csm;
                comBuffer[1][2] = comSumsTotal.sum_ssm;
                comBuffer[2][0] = comSumsTotal.sum_cmp;
                comBuffer[2][1] = comSumsTotal.sum_smp;
                comBuffer[2][2] = 0;
            }
        }
        else
        {
            clear_dvec(comBuffer[0]);
            clear_dvec(comBuffer[1]);
            clear_dvec(comBuffer[2]);
        }
    }

    pullAllReduce(cr, comm, pull->group.size() * c_comBufferStride * DIM,
                  static_cast<double*>(comm->comBuffer[0]));

    for (size_t g = 0; g < pull->group.size(); g++)
    {
        pull_group_work_t* pgrp;

        pgrp = &pull->group[g];
        if (pgrp->needToCalcCom)
        {
            GMX_ASSERT(!pgrp->params.ind.empty(),
                       "Normal pull groups should have atoms, only group 0, which should have "
                       "bCalcCom=FALSE has nat=0");

            const auto comBuffer = gmx::constArrayRefFromArray(
                    comm->comBuffer.data() + g * c_comBufferStride, c_comBufferStride);

            if (pgrp->epgrppbc != epgrppbcCOS)
            {
                double wmass, wwmass;
                int    m;

                /* Determine the inverse mass */
                wmass         = comBuffer[2][0];
                wwmass        = comBuffer[2][1];
                pgrp->mwscale = 1.0 / wmass;
                /* invtm==0 signals a frozen group, so then we should keep it zero */
                if (pgrp->invtm != 0)
                {
                    pgrp->wscale = wmass / wwmass;
                    pgrp->invtm  = wwmass / (wmass * wmass);
                }
                /* Divide by the total mass */
                for (m = 0; m < DIM; m++)
                {
                    pgrp->x[m] = comBuffer[0][m] * pgrp->mwscale;
                    if (xp)
                    {
                        pgrp->xp[m] = comBuffer[1][m] * pgrp->mwscale;
                    }
                    if (pgrp->epgrppbc == epgrppbcREFAT || pgrp->epgrppbc == epgrppbcPREVSTEPCOM)
                    {
                        pgrp->x[m] += comm->pbcAtomBuffer[g][m];
                        if (xp)
                        {
                            pgrp->xp[m] += comm->pbcAtomBuffer[g][m];
                        }
                    }
                }
            }
            else
            {
                /* Cosine weighting geometry */
                double csw, snw, wmass, wwmass;

                /* Determine the optimal location of the cosine weight */
                csw                   = comBuffer[0][0];
                snw                   = comBuffer[0][1];
                pgrp->x[pull->cosdim] = atan2_0_2pi(snw, csw) / twopi_box;
                /* Set the weights for the local atoms */
                wmass  = sqrt(csw * csw + snw * snw);
                wwmass = (comBuffer[1][0] * csw * csw + comBuffer[1][1] * csw * snw
                          + comBuffer[1][2] * snw * snw)
                         / (wmass * wmass);

                pgrp->mwscale = 1.0 / wmass;
                pgrp->wscale  = wmass / wwmass;
                pgrp->invtm   = wwmass / (wmass * wmass);
                /* Set the weights for the local atoms */
                csw *= pgrp->invtm;
                snw *= pgrp->invtm;
                for (size_t i = 0; i < pgrp->atomSet.numAtomsLocal(); i++)
                {
                    int ii                = pgrp->atomSet.localIndex()[i];
                    pgrp->localWeights[i] = csw * std::cos(twopi_box * x[ii][pull->cosdim])
                                            + snw * std::sin(twopi_box * x[ii][pull->cosdim]);
                }
                if (xp)
                {
                    csw                    = comBuffer[2][0];
                    snw                    = comBuffer[2][1];
                    pgrp->xp[pull->cosdim] = atan2_0_2pi(snw, csw) / twopi_box;
                }
            }
            if (debug)
            {
                fprintf(debug, "Pull group %zu wmass %f invtm %f\n", g, 1.0 / pgrp->mwscale, pgrp->invtm);
            }
        }
    }

    if (pull->bCylinder)
    {
        /* Calculate the COMs for the cylinder reference groups */
        make_cyl_refgrps(cr, pull, masses, pbc, t, x);
    }

    if (pull->bSlice or pull->bPoreExpand)
    {
        /* Calculate coordinates for the pore slice pulling coord. This is needed for
           both, pull-geometry = pore-slices or pore-slices+radius */
        pull_pore_slices(cr, pull, pbc, x);
    }

    if (pull->bPoreExpand)
    {
        pull_poreExpansion(cr, pull);
    }
}

/* ****************************
 * FUNCTIONS FOR SLICE-PULLING
 * ****************************/

/**
 * Indicator function, =1 in interval [-1,1] and zero outside, using 3rd-order polynomials as switch function.
 * Mirjalili and Feig, JCTC 2015, eqs. 4 and appendix.
 * Tested that it does what it should. (JSH, Mar 2016).
 */
double switch_func_3rd_order_polynomial(double x, double h, gmx_bool bDerivative)
{
    const double up  =  1;
    const double low = -1;

    if (x <= low - h or x >= up + h)
    {
        return 0.0;
    }
    else if (low + h <= x and x <= up - h)
    {
        return bDerivative ? 0.0 : 1.0;
    }
    else if (up - h <= x and x <= up + h)
    {
        double d = x - up;
        double h3 = h * h * h;

        if (bDerivative)
        {
            double d2 = d * d;

            return -3. / (4 * h) + 3. / (4 * h3) * d2;
        }
        else
        {
            double d3 = d * d * d;

            return 0.5 - 3. / (4. * h) * d + 1. / (4 * h3) * d3;
        }
    }
    else if (low - h <= x and x <= low + h)
    {
        double d = x - low;
        double h3 = h * h * h;

        if (bDerivative)
        {
            double d2 = d * d;

            return 3. / (4. * h) - 3. / (4 * h3) * d2;
        }
        else
        {
            double d3 = d * d * d;

            return 0.5 + 3. / (4. * h) * d - 1. / (4 * h3) * d3;
        }
    }
    else
    {
        fprintf(stderr, "\nWARNING, in switch_func_3rd_order_polynomial(): No evaluation was true: x = %g\n\n", x);

        return 0.0;
    }
}

/**
 * Differentiable heaviside step function, switching with 3rd order polynomial between -h and +h.
 * Tested that it does what it should (JSH, Apr 2020)
 */
static double heaviside_3rd_order_polynomial(double x, double h, gmx_bool bDerivative)
{
    if (x <= -h)
    {
        return 0.0;
    }
    else if (x >= h)
    {
        return bDerivative ? 0.0 : 1.0;
    }
    else
    {
        double h3 = h * h * h;

        if (bDerivative)
        {
            return 0.75 / h - 0.75 / h3 * x * x;
        }
        else
        {
            return 0.5 + 0.75 / h * x - 0.25 / h3 * x * x * x;
        }
    }
}

/**
 * The origial switch function that increases linearly up to
 *   x = 1
 *   y = zeta
 * and then saturates to one with an exponential. The function is continuous and differentiable:
 *
 *          /  a*x            if x <= 1
 *  f(x) = <
 *          \  1-c*exp(-b*x)  if x >  1
 *
 *  where a = zeta. Denoted "Psi" in Hub/Awasthi, JCTC 2017, 13, 2352-2366.
 */
#if 0
static double switch_func_linear_exp_original(double x, double zeta, gmx_bool bDerivative)
{
    if (x < 0)
    {
        gmx_fatal(FARGS, "Found x < 0 in switch_func_linear_exp(), x = %g\n", x);
    }

    double a = zeta;
    double b = a / (1 - a);

    if (x <= 1)
    {
        return (bDerivative ? a : a * x);
    }
    else
    {
        double c = a / b * exp(b);

        if (bDerivative)
        {
            return b * c * exp(-b * x);
        }
        else
        {
            return 1 - c * exp(-b * x);
        }
    }
}
#endif

/**
 * A modified switch function that increases linearly up to
 *   x = gamma
 *   y = zeta
 *  and then saturates to one with an exponential. The function is continuous and differentiable:
 *
 *          /  zeta*x/xiChain  if x <= gamma
 *  f(x) = <
 *          \  1-c*exp(-b*x)   if x >  gamma
 *
 * Denoted "Psi" in Hub/Awasthi, JCTC 2017, 13, 2352-2366, but now with the extra parameter gamma.
*/
double switch_func_linear_exp(double x, double gamma, double zeta, gmx_bool bDerivative)
{
    if (x < 0)
    {
        gmx_fatal(FARGS, "Found x < 0 in switch_func_linear_exp(), x = %g\n", x);
    }

    if (x <= gamma)
    {
        return (bDerivative ? zeta / gamma : zeta / gamma * x);
    }
    else
    {
        double b = zeta / ((1 - zeta) * gamma);
        double c = zeta / (b * gamma) * exp(b * gamma);

        if (bDerivative)
        {
            return b * c * exp(-b * x);
        }
        else
        {
            return 1 - c * exp(-b * x);
        }
    }
}

void pull_slices_z2slicenumbers(real zRelMembrane, pull_t* pull, int* slice0, int* slice1)
{
    /* Note: we need to take slice_h/2, since h acts on the interval -1 to +1, so the range in which
       the 3rd order polynomial acts is -1-h to 1+h. So within *one* slice, this translates to
       switching ranges of h/2 */
    *slice0 = floor(zRelMembrane / pull->slice_d + 1.0 * pull->nslices / 2 - pull->slice_h / 2);
    *slice1 = floor(zRelMembrane / pull->slice_d + 1.0 * pull->nslices / 2 + pull->slice_h / 2);

    if (*slice0 < 0)
    {
        *slice0 = 0;
    }

    if (*slice1 > pull->nslices - 1)
    {
        *slice1 = pull->nslices - 1;
    }
}

/* See: https://en.wikipedia.org/wiki/Center_of_mass#Systems_with_periodic_boundary_conditions */
static inline double pbcCOM_sincos_to_x(double avSin, double avCos, double L)
{
    /* Note: C atanh2(0,0) is defined to 0, so no need to catch this rare case. */
    double theta = atan2(-avSin, -avCos) + M_PI;
    double x = theta / (2 * M_PI) * L;

    return x;
}

#if 0
/* See: https://en.wikipedia.org/wiki/Center_of_mass#Systems_with_periodic_boundary_conditions */
static double pbcCOM(dvec* x, int d, double* v, double L, int N)
{
    double sum_sin = 0, sum_cos = 0, sum_v = 0;

    double twopi_overL = 2 * M_PI / L;

    for (int i = 0; i < N; i++)
    {
        if (v[i] != 0)
        {
            double theta = twopi_overL * x[i][d];
            sum_sin += v[i] * sin(theta);
            sum_cos += v[i] * cos(theta);
            sum_v += v[i];
        }
    }

    return pbcCOM_sincos_to_x(sum_sin / sum_v, sum_cos / sum_v, L);
}
#endif

/*
 * REACTION COORDIANTE FOR SLICE-BASED PULLING OF POLAR ATOMS INTO THE MEMBRANE
 */
void pull_pore_slices(const t_commrec* cr, pull_t* pull, t_pbc* pbc, const rvec x[])
{
    /* Keep a local pointer, can be removed later */
    static long int step = -1;

    if (cr and MASTER(cr))
    {
        step++;
    }

    /* loop over all coords */
    for (int c = 0; c < pull->params.ncoord; c++)
    {
        pull_coord_work_t* pcrd = &pull->coord[c];

        /* Only doing slice/chain coordinte here */
        if (!(pcrd->params.eGeom == epullgSLICES or pcrd->params.eGeom == epullgSLICES_RADIUS))
        {
            continue;
        }

        if (!cr)
        {
            /* We are running grompp. In that case, we don't need to compute our RC,
               so let us just set it to zero */
            printf("\nNOTE: within gmx grompp, the position along the \"pore-slices\" coordindate is not\n"
                   "reported correctly!\n");
            pcrd->xiChain = 0;

            continue;
        }

        /* pref will be the same group for all pull coordinates */
        pull_group_work_t* pref = &pull->group[pcrd->params.group[0]];
        pull_group_work_t* pgrp = &pull->group[pcrd->params.group[1]];

        /* Index and number of local atoms */
        auto localAtomIndices = pgrp->atomSet.localIndex();
        int nat_loc = localAtomIndices.ssize();

        /* Set the Z position of the cylinder */
        double zCylinterCenter;
        if (pull->sliceSetCylinder_z < 0)
        {
            /* Cylinder Z position set to center of mass of pull group 0. This is the common
               setting for pore formation or stalk formation between flat membranes, where
               a COM reference group is availible (given by the membrane core or by the water
               between the two fusing membranes */
            zCylinterCenter = pref->x[ZZ];
            if (cr and MASTER(cr) and 0) {
                fprintf(stderr, "pull group 0, Zcom = %g\n", zCylinterCenter);
            }
        }
        else
        {
            /* Set Z by environmet variable. Needed if the Z position of the cylinder can not be defined
               by the center of mass of a group. Example: Fusion between a flat and a curved membrane */
            zCylinterCenter = pull->sliceSetCylinder_z;
        }


        /* With slice pulling g = 0 is the reference, such as the hydrophobic part of the membrane.
         *
         *  The reactinon coordinate is given via a sum over the slices s:
         *
         *  xiChain  = sum_s delta_s
         *  deltax_s = psi  ( sum_i(atoms) fz[ (z_i - z_s)/(d/2)] frad[ sqrt((ri-rCylCent))/radiusCylinder ]; zeta )
         *                    \_________________  ________________________________________________________/
         *                                      \/
         *                                       = Np[s] (one for each slice)
         *
         *  Here, fswitch is a function that is =1 in [-1,1], and zero outside, but switched smoothly to zero
         *  at the boundaries. zeta controls how many atoms must be added to the slice to consider
         *  the slice as "filled". */

        /* allocate memory, if not yet done */
        if (!pcrd->sum_fz)
        {
            snew(pcrd->sum_fz,          pull->nslices);
            snew(pcrd->Np,              pull->nslices);
            snew(pcrd->wcyl,            pull->nslices);
            snew(pcrd->Ax,              pull->nslices);
            snew(pcrd->Ay,              pull->nslices);
            snew(pcrd->comPBCSlice_cos, pull->nslices);
            snew(pcrd->comPBCSlice_sin, pull->nslices);
            snew(pcrd->comPBCSlice,     pull->nslices);
        }

        if (pcrd->nalloc_grad_xiChain < nat_loc)
        {
            pcrd->nalloc_grad_xiChain = nat_loc + 100;

            srenew(pcrd->gradient_xiChain, pcrd->nalloc_grad_xiChain);
            srenew(pcrd->rRelCyl,          pcrd->nalloc_grad_xiChain);
        }

        /* Compute sum_fz, that is sum_i f_z((zi - zs) / (d / 2)) */
        for (int s = 0; s < pull->nslices; s++)
        {
            pcrd->sum_fz [s] = 0;
            pcrd->Np     [s] = 0;
            pcrd->Ax     [s] = 0;
            pcrd->Ay     [s] = 0;

            clear_dvec(pcrd->comPBCSlice_cos[s]);
            clear_dvec(pcrd->comPBCSlice_sin[s]);
        }

        /* Set pbc center to 0 / 0 / zMembrane before computing */
        rvec x_pbc;
        clear_rvec(x_pbc);
        x_pbc[ZZ] = zCylinterCenter;

        /* Current box - does this work if we don't have a rectangular box? */
        double Lx = pbc->box[XX][XX];
        double Ly = pbc->box[YY][YY];

        if (pbc->box[XX][YY] != 0 or pbc->box[YY][XX] != 0)
        {
            gmx_fatal(FARGS, "Your box does not have a rectangular x-y area (box[XX][YY] = %g  -- box[YY][XX] = %g)\n"
                      "We are not sure if slice pulling (chain coordinate) works with such a box correctly.",
                      pbc->box[XX][YY], pbc->box[YY][XX]);
        }

        /* center of slice s: z_s (relative to membrane center), inline equation page 2354, right column */
        double* zSliceCenter;
        snew(zSliceCenter, pull->nslices);

        for (int s = 0; s < pull->nslices; s++)
        {
            zSliceCenter[s] = (s + 0.5 - 1.0 * pull->nslices / 2) * pull->slice_d;
        }

        double zCylinderBottom = zCylinterCenter + zSliceCenter[0]               - pull->slice_d / 2;
        double zCylinderTop    = zCylinterCenter + zSliceCenter[pull->nslices-1] + pull->slice_d / 2;

        if (cr and MASTER(cr) and (((step%200) == 0)) and 0)
        {
            fprintf(stderr,"Slice pulling: optimal x_pbc = %8.3g %8.3g %8.3g\n", x_pbc[XX], x_pbc[YY], x_pbc[ZZ]);
        }

        #pragma omp parallel for num_threads(pull->nthreads) schedule(static)
        for (int th = 0; th < pull->nthreads; th++)
        {
            int ind_start = (nat_loc * (th + 0)) / pull->nthreads;
            int ind_end   = (nat_loc * (th + 1)) / pull->nthreads;

            /* To keep the code simple, keep these variables local. This causes some malloc/free overhead,
               which should have little effect with the large pull groups of typically a few thousand atoms */
            double* sum_fz_loc;
            dvec* comPBCSlice_cos_loc;
            dvec* comPBCSlice_sin_loc;

            snew(sum_fz_loc,           pull->nslices);
            snew(comPBCSlice_cos_loc,  pull->nslices);
            snew(comPBCSlice_sin_loc,  pull->nslices);

            for (int i = ind_start; i < ind_end; i++)
            {
                int ii = localAtomIndices[i];

                /* Get position relative to x_pbc = 0/0/zCylinterCenter */
                rvec dx;
                pbc_dx(pbc, x[ii], x_pbc, dx);
                double zRelMembrane = dx[ZZ];

                /* Summing only over those slices to which this atom can contribute, that is the atom is within the slice += slice_h */
                int slice0, slice1;
                pull_slices_z2slicenumbers(zRelMembrane, pull, &slice0, &slice1);
                // printf("atom = %d / %d -- z = %g zrel = %g zMem = %g -- slices = %d to %d\n", i, ii, x[ii][2], zRelMembrane, zCylinterCenter, slice0, slice1);

                if (slice0 > slice1)
                {
                    /* This atom is outside all slices, so skip all the following */
                    continue;
                }

                /* Computer center of mass of slice s, taking PBC into account, compute eqs. 21 and 22.
                   Nomenclature: bar(s)_x = comPBCSlice_sin[s][XX]
                   bar(c)_x = comPBCSlice_cos[s][XX]
                   bar(s)_y = comPBCSlice_sin[s][YY]
                   bar(c)_y = comPBCSlice_cos[s][YY] */

                double theta_x = 2 * M_PI * x[ii][XX] / Lx;
                double cos_theta_x = cos(theta_x);
                double sin_theta_x = sin(theta_x);

                double theta_y = 2 * M_PI * x[ii][YY] / Ly;
                double cos_theta_y = cos(theta_y);
                double sin_theta_y = sin(theta_y);

                for (int s = slice0; s <= slice1; s++)
                {
                    /* position of water relative to slice center: (z_i - z_s) / (d/2) */
                    double zRelSliceNorm = (zRelMembrane - zSliceCenter[s]) / (pull->slice_d / 2);

                    double fz = switch_func_3rd_order_polynomial(zRelSliceNorm, pull->slice_h, FALSE);

                    /* Compute F_s, F_s^x, and F_s^y */
                    sum_fz_loc[s] += fz;

                    /* For the COM in a periodic system */
                    comPBCSlice_cos_loc[s][XX] += fz * cos_theta_x;
                    comPBCSlice_sin_loc[s][XX] += fz * sin_theta_x;
                    comPBCSlice_cos_loc[s][YY] += fz * cos_theta_y;
                    comPBCSlice_sin_loc[s][YY] += fz * sin_theta_y;
                }
            }

            #pragma omp critical
            for (int s = 0; s < pull->nslices; s++)
            {
                pcrd->sum_fz[s] += sum_fz_loc[s];
                pcrd->comPBCSlice_cos[s][XX] += comPBCSlice_cos_loc[s][XX];
                pcrd->comPBCSlice_sin[s][XX] += comPBCSlice_sin_loc[s][XX];
                pcrd->comPBCSlice_cos[s][YY] += comPBCSlice_cos_loc[s][YY];
                pcrd->comPBCSlice_sin[s][YY] += comPBCSlice_sin_loc[s][YY];
            }

            sfree(sum_fz_loc);
            sfree(comPBCSlice_cos_loc);
            sfree(comPBCSlice_sin_loc);
        }

        if (cr and PAR(cr))
        {
            pullAllReduce(cr, &pull->comm, pull->nslices, pcrd->sum_fz);
            pullAllReduce(cr, &pull->comm, pull->nslices * 3, static_cast<double*>(pcrd->comPBCSlice_cos[0]));
            pullAllReduce(cr, &pull->comm, pull->nslices * 3, static_cast<double*>(pcrd->comPBCSlice_sin[0]));
        }

        /* Get wcyl[s] = tanh (sum_i fz), used as weights to compute the COM over all slices, Eq. 11 */
        for (int s = 0; s < pull->nslices; s++)
        {
            pcrd->wcyl[s] = tanh(pcrd->sum_fz[s]);
        }

        /*
         * Compute x/y center of each slice, and x/y center of all slices
         */
        clear_dvec(pcrd->comPBCAll_cos);
        clear_dvec(pcrd->comPBCAll_sin);

        double W = 0;

        for (int s = 0; s < pull->nslices; s++)
        {
            W += pcrd->wcyl[s];
        }

        if (W == 0)
        {
            gmx_fatal(FARGS, "Weight sum over slices is zero, W = %g.\n"
                      "This happens if all atoms contributing to the chain/slice coordainte are\n"
                      "above and below the cylinder. Probably, you should use more slices (SLICE_COORD_N).\n", W);
        }

        for (int s = 0; s < pull->nslices; s++)
        {
            double w = pcrd->wcyl[s];

            if (pcrd->wcyl[s] > 0)
            {
                /* X_cyl and Y_cyl */
                /* COM in periodic system, 1/F_s normalization in Eqs. 21 and 22 */
                pcrd->comPBCSlice_cos[s][XX] /= pcrd->sum_fz[s];
                pcrd->comPBCSlice_sin[s][XX] /= pcrd->sum_fz[s];
                pcrd->comPBCSlice_cos[s][YY] /= pcrd->sum_fz[s];
                pcrd->comPBCSlice_sin[s][YY] /= pcrd->sum_fz[s];

                /* sin/cos parts of the total cylinder center of mass, Eqs. 23 and 24 */
                pcrd->comPBCAll_sin[XX] += w/W * pcrd->comPBCSlice_sin[s][XX];
                pcrd->comPBCAll_cos[XX] += w/W * pcrd->comPBCSlice_cos[s][XX];
                pcrd->comPBCAll_sin[YY] += w/W * pcrd->comPBCSlice_sin[s][YY];
                pcrd->comPBCAll_cos[YY] += w/W * pcrd->comPBCSlice_cos[s][YY];

                /* PBC center of mass of slice s, turn back sin/cos representation back to X/Y coordiantes */
                pcrd->comPBCSlice[s][XX] = pbcCOM_sincos_to_x(pcrd->comPBCSlice_sin[s][XX], pcrd->comPBCSlice_cos[s][XX], Lx);
                pcrd->comPBCSlice[s][YY] = pbcCOM_sincos_to_x(pcrd->comPBCSlice_sin[s][YY], pcrd->comPBCSlice_cos[s][YY], Ly);
            }
            else
            {
                /* Set NaN, to make clear that the COM is not defined if no atoms are inside the this slice */
                pcrd->comPBCSlice_cos [s][XX] = NAN;
                pcrd->comPBCSlice_sin [s][XX] = NAN;
                pcrd->comPBCSlice_cos [s][YY] = NAN;
                pcrd->comPBCSlice_sin [s][YY] = NAN;
                pcrd->comPBCSlice     [s][XX] = NAN;
                pcrd->comPBCSlice     [s][YY] = NAN;
            }
        }
 
        /* pbc COM of all slices */

        /* Check if cylincer is currently frozen */
        gmx_bool bCylinderFrozen = (pull->bSliceFreezeCylinder || pcrd->value_ref < pull->sliceFreezeCylinderXiRef);
        if (!bCylinderFrozen)
        {
            /* Eq. 12, Xcyl and Ycyl */
            pcrd->comPBCAll[XX] = pbcCOM_sincos_to_x(pcrd->comPBCAll_sin[XX], pcrd->comPBCAll_cos[XX], Lx);
            pcrd->comPBCAll[YY] = pbcCOM_sincos_to_x(pcrd->comPBCAll_sin[YY], pcrd->comPBCAll_cos[YY], Ly);
        }
        else
        {
            if (pull->sliceFreezeCylinder_x < 0)
            {
                /* pull->sliceFreezeCylinder_x < 0: Freezing cylinder to the position of the first frame */
                pull->sliceFreezeCylinder_x = pbcCOM_sincos_to_x(pcrd->comPBCAll_sin[XX], pcrd->comPBCAll_cos[XX], Lx);
                pull->sliceFreezeCylinder_y = pbcCOM_sincos_to_x(pcrd->comPBCAll_sin[YY], pcrd->comPBCAll_cos[YY], Ly);

                if (cr and MASTER(cr))
                {
                    fprintf(stderr, "\n\nFirst frame, setting the position of the cylinder:\n");
                    fprintf(stderr, "\tcylinder frozen to (x/y) = %10g / %10g\n\n", pull->sliceFreezeCylinder_x, pull->sliceFreezeCylinder_y);
                }
            }

            /* Cylinder position either from SLICE_COORD_FREEZE_CYLINDER_X / SLICE_COORD_FREEZE_CYLINDER_Y, or
               from the position of the first frame */
            pcrd->comPBCAll[XX] = pull->sliceFreezeCylinder_x;
            pcrd->comPBCAll[YY] = pull->sliceFreezeCylinder_y;
        }

        if (std::isnan(pcrd->comPBCAll[XX]) or std::isnan(pcrd->comPBCAll[YY]))
        {
            gmx_fatal(FARGS, "Center of cylinder in NAN: X / Y = %g / %g\n", pcrd->comPBCAll[XX], pcrd->comPBCAll[YY]);
        }

        /* Keep 1/(s^2 + c^2) */
        pcrd->comPBCAll_oneOver_s2c2[XX] = 1 / (gmx::square(pcrd->comPBCAll_sin[XX]) + gmx::square(pcrd->comPBCAll_cos[XX]));
        pcrd->comPBCAll_oneOver_s2c2[YY] = 1 / (gmx::square(pcrd->comPBCAll_sin[YY]) + gmx::square(pcrd->comPBCAll_cos[YY]));

        /* Cut off 1/(s^2 + c^2), takes effect only when we don't have any pore pore. Violates energy conservation only in very
           rare cases. */
        if (pcrd->comPBCAll_oneOver_s2c2[XX] > pull->comPBCAll_oneOver_s2c2_cutoff)
        {
            pcrd->comPBCAll_oneOver_s2c2[XX] = pull->comPBCAll_oneOver_s2c2_cutoff;
        }

        if (pcrd->comPBCAll_oneOver_s2c2[YY] > pull->comPBCAll_oneOver_s2c2_cutoff)
        {
            pcrd->comPBCAll_oneOver_s2c2[YY] = pull->comPBCAll_oneOver_s2c2_cutoff;
        }

        /* ****************************************************************
         *
         * Compute:
         *   1)   sum_i fz frad
         *   2)   sum[i] fz(i) * frad'(i) * 1/Rcyl * (xi-rCylCent)/rc * (-1)
         *
         * ***************************************************************/

        /* Take coordinates relative to COM of cylinder */
        x_pbc[XX] = pcrd->comPBCAll[XX];
        x_pbc[YY] = pcrd->comPBCAll[YY];
        x_pbc[ZZ] = zCylinterCenter;

        #pragma omp parallel for num_threads(pull->nthreads) schedule(static)
        for (int th = 0; th < pull->nthreads; th++)
        {
            int ind_start = (nat_loc * (th + 0)) / pull->nthreads;
            int ind_end   = (nat_loc * (th + 1)) / pull->nthreads;

            /* Again, keep these variables local to keep the code simple (accepting some malloc/free overhead) */
            double *Np_loc, *Ax_loc, *Ay_loc;

            snew(Np_loc, pull->nslices);
            snew(Ax_loc, pull->nslices);
            snew(Ay_loc, pull->nslices);

            for (int i = ind_start; i < ind_end; i++)
            {
                int ii = localAtomIndices[i];

                /* Get position relative to the cylinder center and to the center of the membrane x_pbc = xCyl/yCyl/zCylinterCenter  */
                rvec dx;
                pbc_dx(pbc, x[ii], x_pbc, dx);

                /* Store for force calcualtions later */
                pcrd->rRelCyl[i][XX] = dx[XX];
                pcrd->rRelCyl[i][YY] = dx[YY];
                pcrd->rRelCyl[i][ZZ] = dx[ZZ];

                double zRelMembrane = dx[ZZ];

                /* Summing only over those slices to which this atom can contribute, that is the atom is within the slice += slice_h */
                int slice0, slice1;
                pull_slices_z2slicenumbers(zRelMembrane, pull, &slice0, &slice1);

                /* if this atom is outside all slices, slice0>slice1, so the loop will be skipped */
                for (int s = slice0; s<= slice1; s++)
                {
                    /* position of water relative to slice center: (z_i - z_s) / (d/2) */
                    double zRelSliceNorm = (zRelMembrane - zSliceCenter[s]) / (pull->slice_d / 2);
                    double rc = sqrt(gmx::square(pcrd->rRelCyl[i][XX]) + gmx::square(pcrd->rRelCyl[i][YY]));

                    /* Eqs. 7 and 8 */
                    double fz        = switch_func_3rd_order_polynomial(zRelSliceNorm, pull->slice_h, FALSE);
                    double frad      = switch_func_3rd_order_polynomial(rc / pull->slice_cyl_r, pull->slice_cyl_w, FALSE);
                    double dfrad_drc = switch_func_3rd_order_polynomial(rc / pull->slice_cyl_r, pull->slice_cyl_w, TRUE) / pull->slice_cyl_r;

                    /* Sum up number of polar atoms in slice s, Eqs. 4 and 5 */
                    Np_loc[s] += fz * frad;

                    if (rc > 0)
                    {
                        /* Compute (Eq. 20):
                         *   A_s^x = sum[i] fz(i) * dfrad / drc(i) * (xi - xCylCent) / rc
                         *   A_s^y = sum[i] fz(i) * dfrad / drc(i) * (yi - yCylCent) / rc
                         * These are needed to computed dfrad(r_j)/dri, see apply_forces_grp_slices() in pull.c */
                        Ax_loc[s] += fz * dfrad_drc * pcrd->rRelCyl[i][XX] / rc;
                        Ay_loc[s] += fz * dfrad_drc * pcrd->rRelCyl[i][YY] / rc;
                    }
                }
            }

            #pragma omp critical
            for (int s = 0; s < pull->nslices; s++)
            {
                pcrd->Np[s] += Np_loc[s];
                pcrd->Ax[s] += Ax_loc[s];
                pcrd->Ay[s] += Ay_loc[s];
            }

            sfree(Np_loc);
            sfree(Ax_loc);
            sfree(Ay_loc);
        }

        if (cr and PAR(cr))
        {
            pullAllReduce(cr, &pull->comm, pull->nslices, pcrd->Np);
            pullAllReduce(cr, &pull->comm, pull->nslices, pcrd->Ax);
            pullAllReduce(cr, &pull->comm, pull->nslices, pcrd->Ay);
        }

        /*
         * Finally, compute the reaction coordiante xi, Eq. 1
         */
        pcrd->xiChain = 0;

        for (int s = 0; s < pull->nslices; s++)
        {
            pcrd->xiChain += switch_func_linear_exp(pcrd->Np[s], pull->slice_gamma, pull->slice_zeta, FALSE);
        }

        pcrd->xiChain /= pull->nslices;

        /* Some output */
        if (cr and MASTER(cr) and (((step % pull->sliceNstOutput) == 0)))
        {
            fflush(stdout);
            fprintf(stderr, "\n\nSlice pulling, step %ld, xiChain = %10g, pull coord no %d, Zref = %10g\n", step, pcrd->xiChain, c, zCylinterCenter);
            fprintf(stderr, "----------------------------------------------------\n");
            fprintf(stderr, "%8s  %9s  %9s   %8s %8s\n",       "Slice", "N_p", "psi(N_p,z)", "ComPBCx", "ComPBCy");

            for (int s = 0; s < pull->nslices; s++)
            {
                fprintf(stderr, "%8d  %9.5g  %9.5g   %8.3f %8.3f\n", s, pcrd->Np[s],
                        switch_func_linear_exp(pcrd->Np[s], pull->slice_gamma, pull->slice_zeta, FALSE), pcrd->comPBCSlice[s][XX], pcrd->comPBCSlice[s][YY]);
            }

            fprintf(stderr, "----------------------------------------------------\n");
            fprintf(stderr, "%8s  %9s  %9.5g   %8.3f %8.3f %s\n\n", "TOTAL", "", pcrd->xiChain, pcrd->comPBCAll[XX], pcrd->comPBCAll[YY],
                    bCylinderFrozen ? "(frozen)" : "");
            fprintf(stderr, "\nCylinder center at (X/Y/Z):          %10g %10g %10g\n", pcrd->comPBCAll[XX], pcrd->comPBCAll[YY], zCylinterCenter);
            fprintf(stderr,   "Cylinder spanning z-range (from/to): %10g %10g\n", zCylinderBottom, zCylinderTop);
            fprintf(stderr,   "Cylinder X/Y COM, 1/(s2+c2):         %10g %10g\n\n", pcrd->comPBCAll_oneOver_s2c2[XX], pcrd->comPBCAll_oneOver_s2c2[YY]);
        }

        if (cr and MASTER(cr) and 0)
        {
            /* Some output for debugging */
            fprintf(stderr, "\nnode %2d, Slice xiChain = %g\n\n", cr->nodeid, pcrd->xiChain);

            for (int s = 0; s < pull->nslices; s++)
            {
                fprintf(stderr, "\nnode %d, slice %2d, sum fz      = %8.3g \n", cr->nodeid, s, pcrd->sum_fz[s]);
                fprintf(stderr, "\nnode %d, slice %2d, sum fz frad = %8.3g \n", cr->nodeid, s, pcrd->Np[s]);
            }
        }


        /************************************************************
         *
         *  START of computing Gradient of xiChain wrt. atomic coordinates
         *
         ************************************************************/
        double* psi;
        double* psi_prime;
        snew(psi, pull->nslices);
        snew(psi_prime, pull->nslices);

        for (int s = 0; s < pull->nslices; s++)
        {
            psi[s] = switch_func_linear_exp(pcrd->Np[s], pull->slice_gamma, pull->slice_zeta, FALSE);
            psi_prime[s] = switch_func_linear_exp(pcrd->Np[s], pull->slice_gamma, pull->slice_zeta, TRUE);
            // printf("s = %2d - z = %8.3g - sum_fz = %8.3g Np = %8.3f  tanh = %8.3g\n", s, zSliceCenter[s], pcrd->sum_fz[s], pcrd->Np[s], tanhTerm[s] );
        }

        #pragma omp parallel for num_threads(pull->nthreads) schedule(static)
        for (int th = 0; th < pull->nthreads; th++)
        {
            int ind_start = (nat_loc * (th + 0)) / pull->nthreads;
            int ind_end   = (nat_loc * (th + 1)) / pull->nthreads;

            for (int i = ind_start; i < ind_end; i++)
            {
                double xRelCyl = pcrd->rRelCyl[i][XX];  /* Position relative to cylinder center */
                double yRelCyl = pcrd->rRelCyl[i][YY];
                double zRelMembrane = pcrd->rRelCyl[i][ZZ];  /* Position relative to membrane center */

                clear_dvec(pcrd->gradient_xiChain[i]);

                /* Summing only over those slices to which this atom can contribute, that is the atom is within the slice += slice_h */
                int slice0, slice1;
                pull_slices_z2slicenumbers(zRelMembrane, pull, &slice0, &slice1);

                if (slice0 > slice1)
                {
                    /* This atom is outside all slices, so skip all the following */
                    continue;
                }

                /* *****************************************************************
                 *
                 * Get derivative of X/Y center of cylinder wrt. position of atom i
                 *
                 * ****************************************************************/

                double dXcyl_sin_dz = 0;
                double dXcyl_cos_dz = 0;
                double dYcyl_sin_dz = 0;
                double dYcyl_cos_dz = 0;

                double dXcyl_sin_dx = 0;
                double dXcyl_cos_dx = 0;
                double dYcyl_sin_dy = 0;
                double dYcyl_cos_dy = 0;

                /* x/y coords of this atom, and mapped onto the unit circle */
                double x = pcrd->comPBCAll[XX] + xRelCyl;
                double xTheta = 2 * M_PI * x / Lx;
                double y = pcrd->comPBCAll[YY] + yRelCyl;
                double yTheta = 2 * M_PI * y / Ly;

                double sin_xTheta = sin(xTheta);
                double cos_xTheta = cos(xTheta);
                double sin_yTheta = sin(yTheta);
                double cos_yTheta = cos(yTheta);

                for (int s = slice0; s <= slice1; s++)
                {
                    double w = pcrd->wcyl[s];

                    if (w > 0)
                    {
                        double zRelSliceNorm = (zRelMembrane - zSliceCenter[s]) / (pull->slice_d / 2);

                        double fz     = switch_func_3rd_order_polynomial(zRelSliceNorm, pull->slice_h, FALSE);
                        double dfz_dz = switch_func_3rd_order_polynomial(zRelSliceNorm, pull->slice_h, TRUE) / (pull->slice_d / 2);

                        double dw_dz = (1 - gmx::square(pcrd->wcyl[s])) * dfz_dz;

                        /* derivative of the sin/cos parts of the cylinder COM in this slice s */
                        double tmp = 1 / pcrd->sum_fz[s] * dfz_dz;
                        double ds_x_dz = tmp * (sin_xTheta - pcrd->comPBCSlice_sin[s][XX]);
                        double dc_x_dz = tmp * (cos_xTheta - pcrd->comPBCSlice_cos[s][XX]);

                        double ds_y_dz = tmp * (sin_yTheta - pcrd->comPBCSlice_sin[s][YY]);
                        double dc_y_dz = tmp * (cos_yTheta - pcrd->comPBCSlice_cos[s][YY]);

                        double ds_x_dx = +fz * cos_xTheta * 2 * M_PI / Lx;
                        double dc_x_dx = -fz * sin_xTheta * 2 * M_PI / Lx;

                        double ds_y_dy = +fz * cos_yTheta * 2 * M_PI / Ly;
                        double dc_y_dy = -fz * sin_yTheta * 2 * M_PI / Ly;

                        /* derivative of the sin/cos parts of the overall cylinder COM, Eqs. 31-33 */
                        dXcyl_sin_dz += 1 / W * (w * ds_x_dz + dw_dz * (pcrd->comPBCSlice_sin[s][XX] - pcrd->comPBCAll_sin[XX]));
                        dXcyl_cos_dz += 1 / W * (w * dc_x_dz + dw_dz * (pcrd->comPBCSlice_cos[s][XX] - pcrd->comPBCAll_cos[XX]));

                        dYcyl_sin_dz += 1 / W * (w * ds_y_dz + dw_dz * (pcrd->comPBCSlice_sin[s][YY] - pcrd->comPBCAll_sin[YY]));
                        dYcyl_cos_dz += 1 / W * (w * dc_y_dz + dw_dz * (pcrd->comPBCSlice_cos[s][YY] - pcrd->comPBCAll_cos[YY]));

                        dXcyl_sin_dx += 1 / W * w * ds_x_dx;
                        dXcyl_cos_dx += 1 / W * w * dc_x_dx;

                        dYcyl_sin_dy += 1 / W * w * ds_y_dy;
                        dYcyl_cos_dy += 1 / W * w * dc_y_dy;
                    }
                }

                /* Map the derivative of the sin/cos parts back to the box 
                   Use: d/da atan2(-s,-c) = 1/(s^2+c^2) * (-s dc/da + c*ds/da)

                   This yields finally the derivative of the cylinder X/Y wrt. to x/y/z of atom i
                */
                double dXcyl_dz = Lx / (2 * M_PI) * pcrd->comPBCAll_oneOver_s2c2[XX] * (-pcrd->comPBCAll_sin[XX] * dXcyl_cos_dz + pcrd->comPBCAll_cos[XX] * dXcyl_sin_dz);
                double dYcyl_dz = Ly / (2 * M_PI) * pcrd->comPBCAll_oneOver_s2c2[YY] * (-pcrd->comPBCAll_sin[YY] * dYcyl_cos_dz + pcrd->comPBCAll_cos[YY] * dYcyl_sin_dz);

                double dXcyl_dx = Lx / (2 * M_PI) * pcrd->comPBCAll_oneOver_s2c2[XX] * (-pcrd->comPBCAll_sin[XX] * dXcyl_cos_dx + pcrd->comPBCAll_cos[XX] * dXcyl_sin_dx);
                double dYcyl_dy = Ly / (2 * M_PI) * pcrd->comPBCAll_oneOver_s2c2[YY] * (-pcrd->comPBCAll_sin[YY] * dYcyl_cos_dy + pcrd->comPBCAll_cos[YY] * dYcyl_sin_dy);

                if (pull->bSliceApplyCylForce == 0 or pull->bSliceFreezeCylinder)
                {
                    /* To 
                       i)  test the effect of the force contribution due to the derivative of the cylinder X/Y wrt. to atomic coordinates
                       ii) when freezing the cylinder xy to SLICE_COORD_FREEZE_CYLINDER_X / SLICE_COORD_FREEZE_CYLINDER_Y:
                       Simply turn off those forces: 
                    */
                    dXcyl_dz = dYcyl_dz = 0;
                    dXcyl_dx = dYcyl_dy = 0;
                }

                /* **************************************************************
                 *
                 * Now get derivative of reaction coordinate wrt. atomic position
                 *
                 * *************************************************************/

                for (int s = slice0; s <= slice1; s++)
                {
                    /* position of water relative to slice center: (z_i - z_s) / (d/2) */
                    double zRelSliceNorm = (zRelMembrane - zSliceCenter[s]) / (pull->slice_d / 2);
                    double rc = sqrt(gmx::square(xRelCyl) + gmx::square(yRelCyl));

                    /* Derivatives of the switch functions fz (or faxial) and fradial */
                    double fz        = switch_func_3rd_order_polynomial(zRelSliceNorm, pull->slice_h, FALSE);
                    double dfz_dz    = switch_func_3rd_order_polynomial(zRelSliceNorm, pull->slice_h, TRUE) / (pull->slice_d / 2);
                    double frad      = switch_func_3rd_order_polynomial(rc/pull->slice_cyl_r, pull->slice_cyl_w, FALSE);
                    double dfrad_drc = switch_func_3rd_order_polynomial(rc/pull->slice_cyl_r, pull->slice_cyl_w, TRUE) / pull->slice_cyl_r;

                    /* Deriv. wrt. z[i]. Contribution due to moving this atom out of the slice s */
                    double sum_i_frad_dfz_dz = frad * dfz_dz;

                    double sum_i_fz_dfrad_dx, sum_i_fz_dfrad_dy, sum_i_fz_dfrad_dz;

                    if (rc > 0)
                    {
                        /* Deriv. wrt. x[i] and y[i]
                           Term1: Contribution due to moving this atom i with respect to center of cylinder
                           Term2: Contribution due to moving the cylinder center when moving this atom i

                           For most atoms in most frames, Term1 is much larer than Term2 (as one may expect)
                        */
                        sum_i_fz_dfrad_dx = fz * dfrad_drc * xRelCyl / rc - dXcyl_dx * pcrd->Ax[s];
                        sum_i_fz_dfrad_dy = fz * dfrad_drc * yRelCyl / rc - dYcyl_dy * pcrd->Ay[s];

                        /* Deriv. wrt. z[i]. Contribution due to changing the cylinder center when moving this atom. */
                        sum_i_fz_dfrad_dz = - dXcyl_dz * pcrd->Ax[s]
                                            - dYcyl_dz * pcrd->Ay[s];
                    }
                    else
                    {
                        sum_i_fz_dfrad_dx = sum_i_fz_dfrad_dy = sum_i_fz_dfrad_dz = 0;
                    }

                    /* Derivative of reaction coordiante xiChain wrt. position of this atom, dxiChain/dx, dxiChain/dy, dxiChain/dz */
                    double prefact = 1.0 / pull->nslices * psi_prime[s];
                    pcrd->gradient_xiChain[i][ZZ] += prefact * (sum_i_frad_dfz_dz + sum_i_fz_dfrad_dz);
                    pcrd->gradient_xiChain[i][XX] += prefact * sum_i_fz_dfrad_dx;
                    pcrd->gradient_xiChain[i][YY] += prefact * sum_i_fz_dfrad_dy;
                }

                for (int dd1 = 0; dd1 < DIM; dd1++)
                {
                    if (std::isnan(pcrd->gradient_xiChain[i][dd1]) or std::isinf(pcrd->gradient_xiChain[i][dd1]))
                    {
                        gmx_fatal(FARGS, "In slice pulling code, gradient of xiChain contains NaN: %g %g %g\n",
                                  pcrd->gradient_xiChain[i][XX], pcrd->gradient_xiChain[i][YY], pcrd->gradient_xiChain[i][ZZ]);
                    }
                }

            } /* loop over atoms in this thread */
        } /* loop over OpenMP threads */

        /*
         *   END of computing Gradient of xiChain wrt. atomic coordinates
         */

        sfree(psi);
        sfree(psi_prime);
        sfree(zSliceCenter);
    } /* loop over pull coords */
}


/* For pore expansion: estimate number of polar atoms that are needed within the layer
   such that xiChain ~= xiChainSwitch. This will be used to get the parameter R0, 
   giving the pore radius at which the pore expansion takes over from the pore nucleation
   (pulling on pore radius takes over from slice pulling)

   Assumption: slices (used in slice pulling) outside of the layer (used in radius pulling)
               are assumed to be filled by water, hence contribute 1 to the chain coordinate.
               Slices inside the layer are filled up one by one until the chain coordinate
               takes a value close to xiChainSwitch.

   Result: This way, we get a too small estimate for R0, because slices are in reality not
           filled up one-by-one.
*/
// static void
// compute_R0_poreExpansion_one_by_one(t_commrec *cr, pull_t *pull)
// {
//     double  nPolar = 0, nSliceInLayer, xiChain = 0, xiChainLast = 0, corrFact, *nInSlice;
//     double  nPolarInterp, nPolarCorr;
//     int     iToAdd, nSliceInLayerInt;

//     if (pull->poreExpansion_H > pull->slice_d*pull->nslices)
//     {
//         gmx_fatal(FARGS, "Pulling with pore expansion: Thickness of layer (%g) should not be larger than the cylinder used for slice pulling\n"
//                   "(%d slices, thickness %g)", pull->poreExpansion_H, pull->nslices, pull->slice_d);
//     }

//     nSliceInLayer    = pull->poreExpansion_H / pull->slice_d;
//     nSliceInLayerInt = int(pull->poreExpansion_H / pull->slice_d + 0.5);
//     corrFact         = nSliceInLayer/nSliceInLayerInt;                    /* correction factor close to 1 */

//     if (pull->xiChainSwitch > 0.999 || pull->xiChainSwitch < 0.5)
//     {
//         gmx_fatal(FARGS, "environment variable PORE_EXPANSION_XI_SWITCH should be approximately 0.9 or 0.925. Found %g\n", pull->xiChainSwitch);
//     }

//     snew(nInSlice, nSliceInLayerInt);
//     iToAdd = 0;
//     while (xiChain < pull->xiChainSwitch)
//     {
//         nInSlice[iToAdd] += 1;
//         nPolar           += 1;

//         xiChainLast = xiChain;
//         xiChain     = 0;
//         for (int s = 0; s < nSliceInLayerInt; s++)
//         {
//             xiChain += switch_func_linear_exp(nInSlice[s], pull->slice_gamma, pull->slice_zeta, FALSE);
//         }
//         /* Assuming that slices outside of the layer are completely filled, hence contributing 1 */
//         xiChain += pull->nslices - nSliceInLayerInt;

//         /* normalize by total number of slices */
//         xiChain /= pull->nslices;
//         fprintf(stderr, "nP %g, nPcorr %g - xiCh %g  last %g (outside added %d) - R0 = %g\n",
//                 nPolar, nPolar*corrFact, xiChain, xiChainLast, pull->nslices - nSliceInLayerInt,
//                 sqrt(nPolar*corrFact * pull->poreExpansion_v0 / (M_PI * pull->poreExpansion_H)) );

//         iToAdd = (iToAdd+1)%nSliceInLayerInt;

//         if (nPolar > 10000)
//         {
//             gmx_fatal(FARGS, "Too many interations in compute_R0_poreExpansion(). Something went wrong.\n"
//                       "Maybe strange parameters for slice thickness (%g), gamma (%g), or zeta (%g), or for height of the layer (%g)?",
//                       pull->slice_h, pull->slice_gamma, pull->slice_zeta, pull->poreExpansion_H);
//         }
//     }

//     /* Interpolate between xiChainLast and xiChain to get best estimate, then compute R0 */
//     nPolarInterp           = nPolar - 1 + (pull->xiChainSwitch - xiChainLast)/(xiChain - xiChainLast);
//     nPolarCorr             = nPolarInterp*corrFact;
//     pull->poreExpansion_R0 = sqrt(nPolarCorr * pull->poreExpansion_v0 / (M_PI * pull->poreExpansion_H));

//     if (!cr || (cr && MASTER(cr)))
//     {
//         // fprintf(stderr, "\nXX %g, %g %g\n", nPolar, nPolarInterp, nPolarCorr);
//         fprintf(stderr, "\nDetecting parameter R0 for pore expansion:\n"
//                 "\twith %g polar atoms in layer, xiChain ~= xiChainSwitch (%g)\n"
//                 "\tR0 = %g\n\n", nPolarCorr, pull->xiChainSwitch, pull->poreExpansion_R0);
//     }

//     sfree(nInSlice);
// }

/**
 * For pore expansion: estimate number of polar atoms that are needed within the layer
 * such that xiChain ~= xiChainSwitch. This will be used to get the parameter R0,
 * giving the pore radius at which the pore expansion takes over from the pore nucleation
 * (pulling on pore radius takes over from slice pulling)
 *
 * Assumption: slices (used in slice pulling) outside of the layer (used in radius pulling)
 *             are assumed to be filled by water, hence contribute 1 to the chain coordinate.
 *             Slices inside the layer are filled up randomly until the chain coordinate
 *             takes a value close to xiChainSwitch-xiChainSwitchDelta. This is averaged over 1000 rounds.
 *
 * Result:     This way, we seem to get a very good estimate for R0.
 *             (tested with a 128-DMPC system, JSH, Apr. 2020)
 */
static void compute_R0_poreExpansion(const t_commrec* cr, pull_t* pull)
{
    /* Number of rounds of filling up the slices randomly, used to compute the average */
    const int nRounds = 10000;

    srand((unsigned)3);

    if (pull->poreExpansion_H > pull->slice_d*pull->nslices)
    {
        gmx_fatal(FARGS, "Pulling with pore expansion: Thickness of layer (%g) should not be larger than the cylinder used for slice pulling\n"
                  "(%d slices, thickness %g)", pull->poreExpansion_H, pull->nslices, pull->slice_d);
    }

    double nSliceInLayer = pull->poreExpansion_H / pull->slice_d;
    int nSliceInLayerInt = int(pull->poreExpansion_H / pull->slice_d + 0.5);
    double corrFact = nSliceInLayer / nSliceInLayerInt; /* correction factor close to 1 */

    if (pull->xiChainSwitch > 0.999 || pull->xiChainSwitch < 0.5)
    {
        gmx_fatal(FARGS, "environment variable PORE_EXPANSION_XI_SWITCH should be approximately 0.9 or 0.925. Found %g\n", pull->xiChainSwitch);
    }

    double* nInSlice;
    snew(nInSlice, nSliceInLayerInt);

    double nPolarAver = 0;

    for (int i = 0; i < nRounds; i++)
    {
        /* Clear slices */
        for (int s = 0; s < nSliceInLayerInt; s++)
        {
            nInSlice[s] = 0;
        }

        double xiChain     = 0;
        double xiChainLast = 0;
        double nPolar      = 0;

        while (xiChain < (pull->xiChainSwitch - pull->xiChainSwitchDelta))
        {
            /* Adding one polar atom to random slice */
            int iToAdd = rand() % nSliceInLayerInt;
            nInSlice[iToAdd] += 1;
            nPolar           += 1;
            
            xiChainLast = xiChain;

            /* Get new xiChain */
            xiChain = 0;

            for (int s = 0; s < nSliceInLayerInt; s++)
            {
                xiChain += switch_func_linear_exp(nInSlice[s], pull->slice_gamma, pull->slice_zeta, FALSE);
            }

            /* Assuming that slices outside of the layer are completely filled, hence contributing 1 */
            xiChain += pull->nslices - nSliceInLayerInt;

            /* normalize by total number of slices */
            xiChain /= pull->nslices;
            /* fprintf(stderr, "nP %g, nPcorr %g - xiCh %g  last %g (outside added %d) - R0 = %g\n",
                    nPolar, nPolar*corrFact, xiChain, xiChainLast, pull->nslices - nSliceInLayerInt,
                    sqrt(nPolar*corrFact * pull->poreExpansion_v0 / (M_PI * pull->poreExpansion_H))); */

            if (nPolar > 10000)
            {
                gmx_fatal(FARGS, "Too many interations in compute_R0_poreExpansion(). Something went wrong.\n"
                          "Maybe strange parameters for slice thickness (%g), gamma (%g), or zeta (%g), or for height of the layer (%g)?",
                          pull->slice_h, pull->slice_gamma, pull->slice_zeta, pull->poreExpansion_H);
            }
        }

        /* Interpolate between xiChainLast and xiChain to get best estimate, then compute R0 */
        double nPolarInterp = nPolar - 1 + (pull->xiChainSwitch - xiChainLast) / (xiChain - xiChainLast);
        // printf("Round %4d) nPolar = %g -- nPolarInterp %g \n", i, nPolar, nPolarInterp);
        nPolarAver += nPolarInterp;
    }

    /* Get average, and translate into radius R0 */
    nPolarAver /= nRounds;
    double nPolarCorr = nPolarAver * corrFact;
    pull->poreExpansion_R0 = sqrt(nPolarCorr * pull->poreExpansion_v0 / (M_PI * pull->poreExpansion_H));

    if (!cr or (cr && MASTER(cr)))
    {
        // fprintf(stderr, "\nXX %g, %g %g\n", nPolar, nPolarInterp, nPolarCorr);
        fprintf(stderr, "\nDetecting parameter R0 for pore expansion:\n"
                "\twith %g polar atoms in layer, xiChain ~= xiChainSwitch (%g)\n"
                "\tR0 = %g\n\n", nPolarCorr, pull->xiChainSwitch, pull->poreExpansion_R0);
    }

    sfree(nInSlice);
}


void pull_poreExpansion(const t_commrec* cr, pull_t* pull)
{
    /* Keep a local step, can be removed later */
    static long int step = -1;

    if (cr and MASTER(cr))
    {
        step++;
    }

    if (pull->poreExpansion_R0 == 0)
    {
        if (!cr or MASTER(cr))
        {
            compute_R0_poreExpansion(cr, pull);
        }

        if (cr and PAR(cr))
        {
            gmx_bcast(sizeof(double), &pull->poreExpansion_R0, cr->mpi_comm_mygroup);
        }
    }

    /* loop over all coords */
    for (int c = 0; c < pull->params.ncoord; c++)
    {
        pull_coord_work_t* pcrd = &pull->coord[c];

        pcrd->xiPoreExpand = 0;
        pcrd->nPolar       = 0;
        pcrd->poreR        = 0;

        /* Only doing slice/chain coordinate with radius expansion here */
        if (pcrd->params.eGeom != epullgSLICES_RADIUS)
        {
            continue;
        }

        if (!cr)
        {
            /* We are running grompp. In that case, we don't need to compute our RC */
            printf("\nNOTE: within gmx grompp, the position along the \"pore-slices+radius\" coordindate is not\n"
                   "reported correctly!\n\n");
            continue;
        }

        /* pref will be the same group for all pull coordinates */
        pull_group_work_t* pgrp = &pull->group[pcrd->params.group[1]];

        auto localAtomIndices = pgrp->atomSet.localIndex();
        int nat_loc = localAtomIndices.ssize();

        if (pcrd->nalloc_grad_xiPoreExpand < nat_loc)
        {
            pcrd->nalloc_grad_xiPoreExpand = nat_loc + 100;
            srenew(pcrd->gradient_xiPoreExpand, pcrd->nalloc_grad_xiPoreExpand);
        }

        if (pull->poreExpansion_RmaxRelative > 0)
        {
            pull->poreExpansion_Rmax = pcrd->value_ref * pull->poreExpansion_R0 + pull->poreExpansion_RmaxRelative;
        }
        
        #pragma omp parallel for num_threads(pull->nthreads) schedule(static)
        for (int th = 0; th < pull->nthreads; th++)
        {
            int ind_start = (nat_loc * (th + 0)) / pull->nthreads;
            int ind_end   = (nat_loc * (th + 1)) / pull->nthreads;

            double nPolar_loc = 0;

            for (int i = ind_start; i < ind_end; i++)
            {
                /* The x-y position relative to the cylinder center, and the z-position relative to the membrane center 
                   were computed in pull_pore_slices() and stored in pcrd->rRelCyl[i] */

                double zRelMembrane = pcrd->rRelCyl[i][ZZ];
                double zRelNorm = zRelMembrane / (pull->poreExpansion_H / 2);
                double fz = switch_func_3rd_order_polynomial(zRelNorm, pull->poreExpansion_w, FALSE);

                double frad = 1;

                if (pull->poreExpansion_Rmax > 0)
                {
                    /* Only when RMAX was set, we take only atoms within a cylinder 
                       When RMAX was not set (hence < 0), we take atoms just within a layer */
                    double rc = sqrt(gmx::square(pcrd->rRelCyl[i][XX]) + gmx::square(pcrd->rRelCyl[i][YY]));
                    frad = switch_func_3rd_order_polynomial(rc / pull->poreExpansion_Rmax, pull->poreExpansion_w, FALSE);
                }

                nPolar_loc += fz * frad;
                // dfz_dz = switch_func_3rd_order_polynomial(zRelSliceNorm, pull->slice_h, TRUE) / (pull->poreExpansion / 2);
            }

            /* Sum up the polar atoms in the layer over the threads */
            #pragma omp critical
            pcrd->nPolar += nPolar_loc;
        }

        fflush(stdout);

        /* Sum up polar atoms over the tMPI or MPI processes */
        pullAllReduce(cr, &pull->comm, 1, &pcrd->nPolar);

        /* Pore radius, using: nPolar v0 = pi R^2 H */
        pcrd->poreR = sqrt(pcrd->nPolar * pull->poreExpansion_v0 / (M_PI * pull->poreExpansion_H));

        /* The overall RC:
         *
         *  xi = xi_ch + s[(xi_ch-xi_s); Dxi] * (R-R0)/R0 
         */
        double switchFunc;

        if (pull->frozenSwitch == -1)
        {
            switchFunc = heaviside_3rd_order_polynomial((pcrd->xiChain - pull->xiChainSwitch), pull->xiChainSwitchDelta, FALSE);
        }
        else
        {
            /* Preset value of the switch function, typically set to switch = 1 for wide-open pores with
               environment variable PORE_EXPANSION_FROZEN_SWITCH */
            switchFunc = pull->frozenSwitch;
        }

        pcrd->xiPoreExpand = pcrd->xiChain + switchFunc * (pcrd->poreR - pull->poreExpansion_R0) / pull->poreExpansion_R0;

        /* Some output */
        if (cr and MASTER(cr) and (((step % pull->sliceNstOutput) == 0)))
        {
            fflush(stdout);
            fprintf(stderr,   "PoreExpansion, pull coord no %d\n", c);
            fprintf(stderr,   "PoreExpansion: nP          = %g\n", pcrd->nPolar);
            fprintf(stderr,   "PoreExpansion: R           = %g\n", pcrd->poreR);
            fprintf(stderr,   "PoreExpansion: (R-R0)/R0   = %g\n", (pcrd->poreR - pull->poreExpansion_R0) / pull->poreExpansion_R0);
            fprintf(stderr,   "PoreExpansion: xiChain     = %g\n", pcrd->xiChain);
            fprintf(stderr,   "PoreExpansion: switch      = %g\n", switchFunc);

            if (pull->poreExpansion_RmaxRelative > 0)
            {
                fprintf(stderr,   "PoreExpansion: Rmax      = %g\n", pull->poreExpansion_Rmax);
            }

            fprintf(stderr,   "PoreExpansion: xi          = %g\n\n", pcrd->xiPoreExpand);
        }

        /*
         * Compute the derivates of the overall "slice-pulling+radius" coordinate
         */

        /* Three contributions to gradient of xiPoreExpand:
           Contribution0: grad xi_chain */
            
        /* Contribution1: s'[(xi_ch-xi_s); Dxi] * (R-R0)/R0 * grad xi_ch
           Same prefactor fact1 for all atoms: */

        double switchFunc_prime;

        if (pull->frozenSwitch == -1)
        {
            switchFunc_prime = heaviside_3rd_order_polynomial((pcrd->xiChain - pull->xiChainSwitch), pull->xiChainSwitchDelta, TRUE);
        }
        else
        {
            switchFunc_prime = 0;
        }

        // FILE *fp = fopen("test.dat", "w");
        //
        // for (int j = 0; j <= 1000; j++)
        // {
        //     double x = j/1000.0;
        //     fprintf(fp, "%g %g %g\n", x, heaviside_3rd_order_polynomial((x - pull->xiChainSwitch), pull->xiChainSwitchDelta, FALSE),
        //             heaviside_3rd_order_polynomial((x - pull->xiChainSwitch), pull->xiChainSwitchDelta, TRUE));
        // }
        //
        // fclose(fp);
        //
        // exit(1);

        double fact1 = switchFunc_prime * (pcrd->poreR - pull->poreExpansion_R0) / pull->poreExpansion_R0;

        /* Contribution2: s[(xi_ch-xi_s); Dxi] * 1/R0 * grad R 
           Same prefactor fact2 for all atoms: */

        double fact2 = switchFunc / pull->poreExpansion_R0;

        double fact3;

        if (pcrd->poreR > 0)
        {
            fact3 = 1. / (2 * pcrd->poreR) * pull->poreExpansion_v0 / (M_PI * pull->poreExpansion_H);
        }
        else
        {
            fact3 = 0;
        }

        #pragma omp parallel for num_threads(pull->nthreads) schedule(static)
        for (int th = 0; th < pull->nthreads; th++)
        {
            int ind_start = (nat_loc * (th + 0)) / pull->nthreads;
            int ind_end   = (nat_loc * (th + 1)) / pull->nthreads;

            dvec gradContrib1, gradContrib2 = { 0, 0, 0 };

            for (int i = ind_start; i < ind_end; i++)
            {
                /*
                 * Contribution1: s'[(xi_ch-xi_s) / Dxi] / Dxi * (R - R0) / R0 * grad xi_ch
                 */
                dsvmul(fact1 , pcrd->gradient_xiChain[i], gradContrib1);

                /*
                 * Contribution2: s[(xi_ch-xi_s)/Dxi] * 1/R0 * grad R
                 * Get z-position relative to center of mass of reference group 
                 */
                double zRelMembrane =  pcrd->rRelCyl[i][ZZ];
                double zRelNorm = zRelMembrane / (pull->poreExpansion_H / 2);
                double dfz_dz = switch_func_3rd_order_polynomial(zRelNorm, pull->poreExpansion_w, TRUE) / (pull->poreExpansion_H / 2);

                double dR_dx = 0, dR_dy = 0, frad = 1, dfrad_drc = 0, rc = 1;

                if (pull->poreExpansion_Rmax > 0)
                {
                    /* Only when RMAX was set, we take only atoms within a cylinder
                       When RMAX was not set (hence < 0), we take atoms just within a layer */
                    double fz = switch_func_3rd_order_polynomial(zRelNorm, pull->poreExpansion_w, FALSE);

                    rc = sqrt(gmx::square(pcrd->rRelCyl[i][XX]) + gmx::square(pcrd->rRelCyl[i][YY]));

                    frad      = switch_func_3rd_order_polynomial(rc / pull->poreExpansion_Rmax, pull->poreExpansion_w, FALSE);
                    dfrad_drc = switch_func_3rd_order_polynomial(rc / pull->poreExpansion_Rmax, pull->poreExpansion_w, TRUE) / pull->poreExpansion_Rmax;

                    if (rc > 0)
                    {
                        dR_dx = fact3 * fz * dfrad_drc * pcrd->rRelCyl[i][XX] / rc;
                        dR_dy = fact3 * fz * dfrad_drc * pcrd->rRelCyl[i][YY] / rc;
                    }
                    else
                    {
                        dR_dx = dR_dy = 0;
                    }
                }

                double dR_dz = fact3 * dfz_dz * frad;

                gradContrib2[XX] = fact2 * dR_dx;
                gradContrib2[YY] = fact2 * dR_dy;
                gradContrib2[ZZ] = fact2 * dR_dz;

                /* Sum up three contributions */
                pcrd->gradient_xiPoreExpand[i][XX] = pcrd->gradient_xiChain[i][XX] + gradContrib1[XX] + gradContrib2[XX];
                pcrd->gradient_xiPoreExpand[i][YY] = pcrd->gradient_xiChain[i][YY] + gradContrib1[YY] + gradContrib2[YY];
                pcrd->gradient_xiPoreExpand[i][ZZ] = pcrd->gradient_xiChain[i][ZZ] + gradContrib1[ZZ] + gradContrib2[ZZ];

                /* For debugging, in case of large forces */
                // for (int d=0; d<DIM; d++)
                // {
                //     if ( fabs(pcrd->gradient_xiPoreExpand[i][d]) > 2.0 )
                //     {
                //         fprintf(stderr, "Large gradient %5d %2d %12g %12g %12g %12g\n", i, d, pcrd->gradient_xiPoreExpand[i][d], pcrd->gradient_xiChain[i][d], gradContrib1[d], gradContrib2[d]);
                //         fprintf(stderr, "s s' f1 f2 f3 %10g %10g %10g %10g %10g\n", switchFunc, switchFunc_prime, fact1, fact2, fact3);
                //     }
                // }
            }
        }
    } /* end loop pull coords */
}


using BoolVec = gmx::BasicVector<bool>;

/* Returns whether the pull group obeys the PBC restrictions */
static bool pullGroupObeysPbcRestrictions(const pull_group_work_t& group,
                                          const BoolVec&           dimUsed,
                                          const rvec*              x,
                                          const t_pbc&             pbc,
                                          const gmx::RVec&         x_pbc,
                                          const real               pbcMargin)
{
    /* Determine which dimensions are relevant for PBC */
    BoolVec dimUsesPbc       = { false, false, false };
    bool    pbcIsRectangular = true;
    for (int d = 0; d < pbc.ndim_ePBC; d++)
    {
        if (dimUsed[d])
        {
            dimUsesPbc[d] = true;
            /* All non-zero dimensions of vector v are involved in PBC */
            for (int d2 = d + 1; d2 < pbc.ndim_ePBC; d2++)
            {
                assert(d2 < DIM);
                if (pbc.box[d2][d] != 0)
                {
                    dimUsesPbc[d2]   = true;
                    pbcIsRectangular = false;
                }
            }
        }
    }

    rvec marginPerDim    = {};
    real marginDistance2 = 0;
    if (pbcIsRectangular)
    {
        /* Use margins for dimensions independently */
        for (int d = 0; d < pbc.ndim_ePBC; d++)
        {
            marginPerDim[d] = pbcMargin * pbc.hbox_diag[d];
        }
    }
    else
    {
        /* Check the total distance along the relevant dimensions */
        for (int d = 0; d < pbc.ndim_ePBC; d++)
        {
            if (dimUsesPbc[d])
            {
                marginDistance2 += pbcMargin * gmx::square(0.5) * norm2(pbc.box[d]);
            }
        }
    }

    auto localAtomIndices = group.atomSet.localIndex();
    for (gmx::index indexInSet = 0; indexInSet < localAtomIndices.ssize(); indexInSet++)
    {
        rvec dx;
        pbc_dx(&pbc, x[localAtomIndices[indexInSet]], x_pbc, dx);

        bool atomIsTooFar = false;
        if (pbcIsRectangular)
        {
            for (int d = 0; d < pbc.ndim_ePBC; d++)
            {
                if (dimUsesPbc[d] && (dx[d] < -marginPerDim[d] || dx[d] > marginPerDim[d]))
                {
                    atomIsTooFar = true;
                }
            }
        }
        else
        {
            real pbcDistance2 = 0;
            for (int d = 0; d < pbc.ndim_ePBC; d++)
            {
                if (dimUsesPbc[d])
                {
                    pbcDistance2 += gmx::square(dx[d]);
                }
            }
            atomIsTooFar = (pbcDistance2 > marginDistance2);
        }
        if (atomIsTooFar)
        {
            return false;
        }
    }

    return true;
}

int pullCheckPbcWithinGroups(const pull_t& pull, const rvec* x, const t_pbc& pbc, real pbcMargin)
{
    if (pbc.pbcType == PbcType::No)
    {
        return -1;
    }

    /* Determine what dimensions are used for each group by pull coordinates */
    std::vector<BoolVec> dimUsed(pull.group.size(), { false, false, false });
    for (size_t c = 0; c < pull.coord.size(); c++)
    {
        const t_pull_coord& coordParams = pull.coord[c].params;
        for (int groupIndex = 0; groupIndex < coordParams.ngroup; groupIndex++)
        {
            for (int d = 0; d < DIM; d++)
            {
                if (coordParams.dim[d] && !(coordParams.eGeom == epullgCYL && groupIndex == 0))
                {
                    dimUsed[coordParams.group[groupIndex]][d] = true;
                }
            }
        }
    }

    /* Check PBC for every group that uses a PBC reference atom treatment */
    for (size_t g = 0; g < pull.group.size(); g++)
    {
        const pull_group_work_t& group = pull.group[g];
        if ((group.epgrppbc == epgrppbcREFAT || group.epgrppbc == epgrppbcPREVSTEPCOM)
            && !pullGroupObeysPbcRestrictions(group, dimUsed[g], x, pbc, pull.comm.pbcAtomBuffer[g], pbcMargin))
        {
            return g;
        }
    }

    return -1;
}

bool pullCheckPbcWithinGroup(const pull_t&                  pull,
                             gmx::ArrayRef<const gmx::RVec> x,
                             const t_pbc&                   pbc,
                             int                            groupNr,
                             real                           pbcMargin)
{
    if (pbc.pbcType == PbcType::No)
    {
        return true;
    }
    GMX_ASSERT(groupNr < gmx::ssize(pull.group), "groupNr is out of range");

    /* Check PBC if the group uses a PBC reference atom treatment. */
    const pull_group_work_t& group = pull.group[groupNr];
    if (group.epgrppbc != epgrppbcREFAT && group.epgrppbc != epgrppbcPREVSTEPCOM)
    {
        return true;
    }

    /* Determine what dimensions are used for each group by pull coordinates */
    BoolVec dimUsed = { false, false, false };
    for (size_t c = 0; c < pull.coord.size(); c++)
    {
        const t_pull_coord& coordParams = pull.coord[c].params;
        for (int groupIndex = 0; groupIndex < coordParams.ngroup; groupIndex++)
        {
            if (coordParams.group[groupIndex] == groupNr)
            {
                for (int d = 0; d < DIM; d++)
                {
                    if (coordParams.dim[d] && !(coordParams.eGeom == epullgCYL && groupIndex == 0))
                    {
                        dimUsed[d] = true;
                    }
                }
            }
        }
    }

    return (pullGroupObeysPbcRestrictions(group, dimUsed, as_rvec_array(x.data()), pbc,
                                          pull.comm.pbcAtomBuffer[groupNr], pbcMargin));
}

void setPrevStepPullComFromState(struct pull_t* pull, const t_state* state)
{
    for (size_t g = 0; g < pull->group.size(); g++)
    {
        for (int j = 0; j < DIM; j++)
        {
            pull->group[g].x_prev_step[j] = state->pull_com_prev_step[g * DIM + j];
        }
    }
}

void updatePrevStepPullCom(struct pull_t* pull, t_state* state)
{
    for (size_t g = 0; g < pull->group.size(); g++)
    {
        if (pull->group[g].needToCalcCom)
        {
            for (int j = 0; j < DIM; j++)
            {
                pull->group[g].x_prev_step[j]          = pull->group[g].x[j];
                state->pull_com_prev_step[g * DIM + j] = pull->group[g].x[j];
            }
        }
    }
}

void allocStatePrevStepPullCom(t_state* state, const pull_t* pull)
{
    if (!pull)
    {
        state->pull_com_prev_step.clear();
        return;
    }
    size_t ngroup = pull->group.size();
    if (state->pull_com_prev_step.size() / DIM != ngroup)
    {
        state->pull_com_prev_step.resize(ngroup * DIM, NAN);
    }
}

void initPullComFromPrevStep(const t_commrec* cr, pull_t* pull, const real* masses, t_pbc* pbc, const rvec x[])
{
    pull_comm_t* comm   = &pull->comm;
    size_t       ngroup = pull->group.size();

    if (!comm->bParticipate)
    {
        return;
    }

    GMX_ASSERT(comm->pbcAtomBuffer.size() == pull->group.size(),
               "pbcAtomBuffer should have size number of groups");
    GMX_ASSERT(comm->comBuffer.size() == pull->group.size() * c_comBufferStride,
               "comBuffer should have size #group*c_comBufferStride");

    pull_set_pbcatoms(cr, pull, x, comm->pbcAtomBuffer);

    for (size_t g = 0; g < ngroup; g++)
    {
        pull_group_work_t* pgrp;

        pgrp = &pull->group[g];

        if (pgrp->needToCalcCom && pgrp->epgrppbc == epgrppbcPREVSTEPCOM)
        {
            GMX_ASSERT(pgrp->params.ind.size() > 1,
                       "Groups with no atoms, or only one atom, should not "
                       "use the COM from the previous step as reference.");

            rvec x_pbc = { 0, 0, 0 };
            copy_rvec(comm->pbcAtomBuffer[g], x_pbc);

            if (debug)
            {
                fprintf(debug, "Initialising prev step COM of pull group %zu. x_pbc =", g);
                for (int m = 0; m < DIM; m++)
                {
                    fprintf(debug, " %f", x_pbc[m]);
                }
                fprintf(debug, "\n");
            }

            /* The following is to a large extent similar to pull_calc_coms() */

            /* The final sums should end up in sum_com[0] */
            ComSums& comSumsTotal = pull->comSums[0];

            if (pgrp->atomSet.numAtomsLocal() <= c_pullMaxNumLocalAtomsSingleThreaded)
            {
                sum_com_part(pgrp, 0, pgrp->atomSet.numAtomsLocal(), x, nullptr, masses, pbc, x_pbc,
                             &comSumsTotal);
            }
            else
            {
#pragma omp parallel for num_threads(pull->nthreads) schedule(static)
                for (int t = 0; t < pull->nthreads; t++)
                {
                    int ind_start = (pgrp->atomSet.numAtomsLocal() * (t + 0)) / pull->nthreads;
                    int ind_end   = (pgrp->atomSet.numAtomsLocal() * (t + 1)) / pull->nthreads;
                    sum_com_part(pgrp, ind_start, ind_end, x, nullptr, masses, pbc, x_pbc,
                                 &pull->comSums[t]);
                }

                /* Reduce the thread contributions to sum_com[0] */
                for (int t = 1; t < pull->nthreads; t++)
                {
                    comSumsTotal.sum_wm += pull->comSums[t].sum_wm;
                    comSumsTotal.sum_wwm += pull->comSums[t].sum_wwm;
                    dvec_inc(comSumsTotal.sum_wmx, pull->comSums[t].sum_wmx);
                    dvec_inc(comSumsTotal.sum_wmxp, pull->comSums[t].sum_wmxp);
                }
            }

            if (pgrp->localWeights.empty())
            {
                comSumsTotal.sum_wwm = comSumsTotal.sum_wm;
            }

            /* Copy local sums to a buffer for global summing */
            auto localSums = gmx::arrayRefFromArray(comm->comBuffer.data() + g * c_comBufferStride,
                                                    c_comBufferStride);

            localSums[0]    = comSumsTotal.sum_wmx;
            localSums[1]    = comSumsTotal.sum_wmxp;
            localSums[2][0] = comSumsTotal.sum_wm;
            localSums[2][1] = comSumsTotal.sum_wwm;
            localSums[2][2] = 0;
        }
    }

    pullAllReduce(cr, comm, ngroup * c_comBufferStride * DIM, static_cast<double*>(comm->comBuffer[0]));

    for (size_t g = 0; g < ngroup; g++)
    {
        pull_group_work_t* pgrp;

        pgrp = &pull->group[g];
        if (pgrp->needToCalcCom)
        {
            if (pgrp->epgrppbc == epgrppbcPREVSTEPCOM)
            {
                auto localSums = gmx::arrayRefFromArray(
                        comm->comBuffer.data() + g * c_comBufferStride, c_comBufferStride);
                double wmass, wwmass;

                /* Determine the inverse mass */
                wmass         = localSums[2][0];
                wwmass        = localSums[2][1];
                pgrp->mwscale = 1.0 / wmass;
                /* invtm==0 signals a frozen group, so then we should keep it zero */
                if (pgrp->invtm != 0)
                {
                    pgrp->wscale = wmass / wwmass;
                    pgrp->invtm  = wwmass / (wmass * wmass);
                }
                /* Divide by the total mass */
                for (int m = 0; m < DIM; m++)
                {
                    pgrp->x[m] = localSums[0][m] * pgrp->mwscale;
                    pgrp->x[m] += comm->pbcAtomBuffer[g][m];
                }
                if (debug)
                {
                    fprintf(debug, "Pull group %zu wmass %f invtm %f\n", g, 1.0 / pgrp->mwscale,
                            pgrp->invtm);
                    fprintf(debug, "Initialising prev step COM of pull group %zu to", g);
                    for (int m = 0; m < DIM; m++)
                    {
                        fprintf(debug, " %f", pgrp->x[m]);
                    }
                    fprintf(debug, "\n");
                }
                copy_dvec(pgrp->x, pgrp->x_prev_step);
            }
        }
    }
}
