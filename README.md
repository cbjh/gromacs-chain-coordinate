# Implementation of the chain coordinate into Gromacs

We present an implementation of the "chain reaction coordinate" into the pull code of GROMACS.

This code is derived from [GROMACS](https://gitlab.com/gromacs/gromacs). This is not official GROMACS.

License: This is free software, distributed under the GNU Lesser General Public License, version 2.1

## The chain coordinante

The *chain reaction coordinate* quantifies the degree of *connectivity* between two compartments. By pulling along the chain coordinate $`\xi_\text{ch}`$ in an MD simulation, you can induce the formation of
- pores (or water defects) over lipid membranes,
- stalks between membranes, the first step of membrane fusion.

Take a look into [documentation-chain-coord](documentation-chain-coord) for examples and documentation.

<p align="center">
<img height="400" src="documentation-chain-coord/Example-stalk-formation/pics/popc-stalk.jpg">
<img height="400" src="documentation-chain-coord/Example-pore-formation/Tiny-test-system/pore-formed.jpg">
</p>


## References

*Probing a continuous polar defect: A reaction coordinate for pore formation in lipid membranes*  
Jochen S. Hub and Neha Awasthi  
J. Chem. Theory Comput., 13, 2352-2366 (2017) [link](http://dx.doi.org/10.1021/acs.jctc.7b00106)

For additional background that motivates the development of the chain coordinate, see:
*Simulations of pore formation in lipid membranes: reaction coordinates, convergence, hysteresis, and finite-size effects*  
Neha Awasthi and Jochen S Hub  
J. Chem. Theory Comput., 12, 3261-3269 (2016) [link](http://dx.doi.org/10.1021/acs.jctc.6b00369)

*Joint reaction coordinate for computing the free energy landscape of pore nucleation and pore expansion in lipid membranes*  
Jochen S. Hub  
J. Chem. Theory Comput., 17, 1229-1239 (2021) [link](https://doi.org/10.1021/acs.jctc.0c01134)

### Applications of $`\xi_\text{ch}`$

*Free energies of membrane stalk formation from a lipidomics perspective*  
Chetan C. Poojari, Katharina S. Scherer, Jochen S. Hub  
Nature Commun., 12, 6594 (2021) [link](http://dx.doi.org/10.1038/s41467-021-26924-2)

*Metastable prepores in tension-free lipid bilayers*  
Christina L. Ting, Neha Awasthi, Marcus Müller, and Jochen S. Hub  
Phys. Rev. Lett., 120, 128103 (2018) [link](https://doi.org/10.1103/PhysRevLett.120.128103)

Chetan S. Poojari, Katharina C. Scherer and Jochen S. Hub  
*Free energies of stalk formation in the lipidomics era*  
BioRxiv, doi: 10.1101/2021.06.02.446700 [link](https://doi.org/10.1101/2021.06.02.446700)

*How arginine derivatives alter the stability of lipid membranes: dissecting the roles of side chains, backbone, and termini*  
Sarah F. Verbeek, Neha Awasthi, Nikolas K. Teiwes, Ingo Mey, Jochen S. Hub\*, Andreas Janshoff\*  
Eur. Biophy. J. 50, 127-142 (2021) [link](https://doi.org/10.1007/s00249-021-01503-x)

*Molecular mechanism of polycation-induced pore formation in biomembranes*  
Neha Awasthi, Wojciech Kopec, Natalia Wilkosz, Dorota Jamróz, Jochen S. Hub, Maria Zatorska, Rafa Petka, Maria Nowakowska, and Mariusz Kepczynski  
ACS Biomater. Sci. Eng., 5, 780-794 (2019)

### Book chapter

*Free energy calculations of pore formation in lipid membranes*  
Neha Awasthi and Jochen S. Hub  
In: Biomembrane Simulations: Computational Studies of Biological Membranes  
Edited by Max Berkowitz
Series in Computational Biophysics, CRC Press Taylor and Francis, Published June 18, 2019 [author manuscript](https://biophys.uni-saarland.de/pub/Awasthi2018.pdf)

## Installation

Normal GROMACS can be installed by 1) downloading source files 2) running CMake and 3) running Make. Similar procedure is valid for GROMACS Chain Coordinate. The only difference are different source files. You can download GROMACS Chain Coordinate from [_Releases_](https://gitlab.com/cbjh/gromacs-chain-coordinate/-/releases), then build it and install in usual way.

Additionally packages for Conda and Spack are available.

More detailed instructions from us can be found here [GROMACS Chain Coordinate installation](https://gitlab.com/cbjh/gromacs-chain-coordinate/-/wikis/GROMACS-Chain-Coordinate-installation).
