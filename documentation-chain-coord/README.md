# Implementation of the chain coordinate into Gromacs

For documentation and an example for pore formation, go to [Example-pore-formation](Example-pore-formation)

For an example of stalk formation, see [Example-stalk-formation](Example-stalk-formation)
