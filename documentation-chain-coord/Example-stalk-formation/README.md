# Stalk formation with the chain coordinate

Pulling along the chain coordinate $`\xi_\text{ch}`$ allows you to form a continuous hydrophobic connection between two lipid membranes, as formed during stalk formation. Here, an example system with the coarse-grained [MARTINI](http://www.cgmartini.nl) force field (version 2.2) is provided composed of two membranes of pure POPC.

For an introduction on the usage of $`\xi_\text{ch}`$ and a list of parameters, take a look at the pore formation example in `Example-pore-formation`.

## Graphic of a stalk between two POPC membranes
<p align="center">
<img width="200" src="pics/popc-stalk-small.jpg">
</p>


## Opening the stalk
The folder `opening` provides the input files for running constant-velocity pulling along $`\xi_\text{ch}`$ over 200ns of simulation. The required commands are listed in `README.sh`.

## Umbrella sampling along $`\xi_\text{ch}`$ for computing a PMF of stalk formation
The folder `umbrella-sampling` contains the starting files for umbrella sampling, using 19 umbrella windows of 200ns each. On a modern Server with a decent GPU, this should take ~20 minutes per window.
- Starting frames `emXXX.gro`
- mdp files `pullXXX.mdp`
- Topology files `topol.top` and `*itp` files
- Reference coordinate file `restraint.gro` for flat-bottomed position restraints
- Index file `index.ndx` that defines the pull groups: group of hydrophobic lipid tail beads `tails` contributing to the connectivity along the stalk and water in the proximal compartment `MiddleWater` used as reference group.

Take a look at `README.sh` for the required commands. The resulting PMF is shown here, computed with [gmx wham](http://dx.doi.org/10.1021/ct100494z) and using the last 150ns of the umbrella windows:

<p align="center">
<img width="400" src="pics/prof.b50.png">
</p>

## References

*Free energies of membrane stalk formation from a lipidomics perspective*
Chetan C. Poojari, Katharina S. Scherer, Jochen S. Hub
Nature Commun., 12, 6594 (2021) [link](http://dx.doi.org/10.1038/s41467-021-26924-2)

JS Hub and N Awasthi  
*Probing a continuous polar defect: A reaction coordinate for pore formation in lipid membranes*  
J. Chem. Theory Comput., 13, 2352-2366 (2017) [link](http://dx.doi.org/10.1021/acs.jctc.7b00106)

