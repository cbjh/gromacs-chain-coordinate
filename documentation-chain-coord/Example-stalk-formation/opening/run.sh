
export SLICE_COORD_D=0.1
export SLICE_COORD_N=17
export SLICE_COORD_ZETA=0.75
export SLICE_COORD_CYL_R=1.2
export SLICE_COORD_NST_OUTPUT=2500
export SLICE_COORD_FREEZE_CYLINDER=0

source /data/users/jhub/projects/pore-formation/build-gromacs-poreExpansion/bin/GMXRC.bash

gmx mdrun -nice 19 -v $GPU1 >& md.lis &
