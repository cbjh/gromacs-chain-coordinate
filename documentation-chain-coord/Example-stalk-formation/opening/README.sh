
source /path/to/Gromacs-with-chain-coordinate/bin/GMXRC.bash

rm -f mdout.mdp topol.tpr
gmx grompp -c lamellar-equil.gro -r restraint.gro -n -f pull.mdp

#
# Set Chain coordinate parameters
#
export SLICE_COORD_N=17               # Number of slices. Important!
                                      # The cylinder should be long enough, such that xi[ch]~0.2 in the state of a flat membrane
export SLICE_COORD_CYL_R=1.2          # Cylinder Radius
export SLICE_COORD_D=0.1              # Slice thickness (default value)
export SLICE_COORD_ZETA=0.75          # Degree to which slice is filled upon addition of the first atom (default value)
export SLICE_COORD_NST_OUTPUT=2500    # Output frequency on chain coord written by mdrun to stdout
export SLICE_COORD_FREEZE_CYLINDER=0  # Dot not freeze cylinder (default), allow cylinder to travel in the membrane plane

rm -f pullx.xvg pullf.xvg *cpt md.log ener.edr traj_comp.xtc confout.gro
gmx mdrun -v

