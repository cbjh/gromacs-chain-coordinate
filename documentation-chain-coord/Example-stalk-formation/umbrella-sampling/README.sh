
source /path/to/Gromacs-with-chain-coordinate/bin/GMXRC.bash

N=19

here=`pwd`
mkdir umbrella-run-dir

for ((i=0; i<N; i++)); do

    nr=$(printf "%03d" $i)

    mkdir umbrella-run-dir/$nr
    rm -f mdout.mdp umbrella-run-dir/$nr/topol.tpr
    cmd="gmx grompp -c em$nr.gro -r restraint.gro -n index.ndx -f pull$nr.mdp -o umbrella-run-dir/$nr/topol.tpr"

    echo -e "Running:\n\n$cmd\n"
    $cmd >& grompp.lis || { cat grompp.lis >&2; break; }

done

# Script to set environment variables for chain coordinate.
# Source this before calling gmx mdrun in your job script
echo "
#
# Source before calling gmx mdrun with
#
# source $here/umbrella-run-dir/chain-coord-settings.sh
#
export SLICE_COORD_N=17        # Number of slices. Important!
export SLICE_COORD_CYL_R=1.2   # Cylinder Radius
export SLICE_COORD_D=0.1       # Slice thickness (default value)
export SLICE_COORD_ZETA=0.75   # Degree to which slice is filled upon addition of the first atom (default value)

" > umbrella-run-dir/chain-coord-settings.sh

