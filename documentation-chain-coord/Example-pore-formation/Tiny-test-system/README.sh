
#
# Replace 
#
source /path/to/Gromacs-with-chain-coordinate/bin/GMXRC.bash

[ -d simdir ] || mkdir simdir

rm -f simdir/topol.tpr simdir/mdout.mdp
gmx grompp -c 32dmpc.pdb -f pull -n -o simdir/topol.tpr -po simdir/mdout.mdp

cd simdir

#
# Set parameters for chain coordinate
#
export SLICE_COORD_N=26            # number of slices. Important!
export SLICE_COORD_D=0.1           # slice thickness in nm (default)
export SLICE_COORD_ZETA=0.75       # degree, to which a slice is filled upon the addition of the
                                   # first atom
export SLICE_COORD_CYL_R=1.2       # Radius if cylinder: The default of 1.2nm is often fine. For very
                                   # floppy membranes, a smaller value can be used (e.g. 0.9)
export SLICE_COORD_NST_OUTPUT=500  # frequency of writing information on chain coordinate to stdout by gmx mdrun

#
# Start the simulation
#
gmx mdrun -v

