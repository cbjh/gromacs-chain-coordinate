# Tiny test system for pore formation over lipid membranes with the chain coordinate

Last update: August 2021

## References
Probing a continuous polar defect: A reaction coordinate for pore formation in lipid membranes  
Jochen S. Hub and Neha Awasthi  
J. Chem. Theory Comput., 13, 2352-2366 (2017) [link](http://dx.doi.org/10.1021/acs.jctc.7b00106)

For additional background, please also read:  
Simulations of pore formation in lipid membranes: reaction coordinates, convergence, hysteresis, and finite-size effects  
Neha Awasthi and Jochen S Hub  
J. Chem. Theory Comput., 12, 3261-3269 (2016) [link](http://dx.doi.org/10.1021/acs.jctc.6b00369)

# Introduction
We present a tiny test system with only 32 DMPC Berger lipids to illustrate and quickly try the use of the chain coordinate  $`\xi_\text{ch}`$ for forming a pore over a lipid membrane. Below, the use of the $`\xi_\text{ch}`$ is explained in some detail. For a quick start, take a look at the script `README.sh` in `Tiny-test-system`.

Important: This test systen contains only 32 lipids, which leads to severe
            PBC artifacts. For production simulations, use a larger system. For relatively thin membranes, such as membranes of DMPC, 128 lipids are enough. For thicker membranes or stiffer membranes containing cholesterol, a slightly larger system is recommended, such as 162 lipids ($`2\times9\times 9`$), to fully exclude PBC artifacts.

# Running a pore-opening simulation

```
# Put the modified Gromacs, which implements the chain coordinate, into your path
source /path/to/Gromacs-chain-coordinate/bin/GMXRC.bash
```

Pulling along $`\xi_\text{ch}`$ is turned on in the pull options of the mdp file. The mdp file `pull.mdp` provided in this example creates a pore within 500ps of simulation. For production simulations, create the
the pore much more slowly, for instance within 20ns to 100ns. The relevant mdp options are:
```
pull                     = yes
pull-group1-name         = tails             ; Reference pull group, here lipid tail atoms. Only used to set the Z position of the cylinder
pull-group2-name         = OW_OA_OB_OC_OD    ; Pull group of polar atoms, here water and phosphate oxygen atoms
pull-coord1-type         = umbrella
pull-coord1-geometry     = pore-slices       ; Use the chain coordinate
pull-coord1-groups       = 1 2
pull-coord1-dim          = N N Y
pull-coord1-init         = 0.15              ; ~0.2 or 0.25 should correspond to the flat, intact membrane. 
                                             ; The number of slices must be chosen accordingly (see below)
```
To pull to a fully formed pore ($`\xi_\text{ch}=1`$) choose: `pull-coord1-rate = (1 - pull-coord1-init)/(dt*nsteps)`. If you pull more slowly you must adapt pull-coord1-rate to reach $`\xi_\text{ch}=1`$ at the end of your simulation.
```
pull-coord1-rate         = 0.0017            ; Pull from xi=0.15 up to xi=1 within 500ps. For production simulations,
                                             ; open the pore more slowly, typically within 50ns or 100ns
pull-coord1-k            = 10000             ; Force constant in units kJ/mol (not kJ/mol/nm2, since xi is unit-less)
```

The index file `index.ndx` contains a group named `OW_OA_OB_OC_OD` that defines the polar heavy atoms contributing to the reaction coordinate, here taken as the water oxygen atoms and the four oxygen atoms of the phosphate
 groups. In addition, `index.ndx` contains a group of lipid tail atoms  that are used as reference.


# Parameters of the chain coordinate

Parameters of the chain coordinate are specified with environment variables. You must specify the number of slices (`SLICE_COORD_N`) and thereby the length of the cylinder. All other parameters may be taken as the default.

## Choosing the number of slices of the cylinder

The number of slices $`N_s`$ are specified with `export SLICE_COORD_N=...`. By specifying  $`N_s`$, you specify the *length* of the cylinder. The length of the cylinder is $`N_s\cdot d_s`$, where $`d_s`$ is the thickness of the slices, typically chosen as 0.1nm (or 1Å) by `SLICE_COORD_D`.

Choose $`N_s`$ such that some
slices are occupied with polar atoms in the *unperturbed* flat membrane (without any pore).
A good choice would be such that the reaction coordinate is $`\xi_\text{ch}\approx 0.25`$
in the flat membrane, meaning that ~25% of the slices are filled by polar atoms at the head group/glycerol regions of the membrane. To find a good value for $`N_s`$ , we recommend
to do a series of reruns (`gmx mdrun -v -rerun traj.xtc`) with different values
of `SLICE_COORD_N`, and then check the average $`\xi_\text{ch}`$ from the `pullx.xvg` output. See an example script below under "Common Errors, Cylinder too short". 

A good choice for `SLICE_COORD_N` strongly
depends on the thickness of the membrane. Good values were 20 to 36 for
membranes between DLPC (very thin) to POPE (quite thick). 

In this example (DMPC), we use
```
export SLICE_COORD_N=26
```

## Radius of the cylinder

A good value for the radius of the cylinder (`SLICE_COORD_CYL_R`) is often `1.2nm`, the default.
Only for very floppy membranes, such as DLPC, DMPG, or DMPG, which carry out
heavy membrane undulations, better use a slightly smaller value such as
```
export SLICE_COORD_CYL_R=0.9
```

*Explanation:* The cylinder ensures that the defect is *localized* in the membrane plane. This avoids that two laterally displaced *partial* defects, one coming from the upper and one from the lower water compartment, are misinterpreted as a continuous polar defect. Such a misinterpretation would lead to undesired hysteresis problems. Such two laterally displaced defects are easier to form for floppy membranes (such as DLPC, DMPG, or DMPG). Hence, a slightly thinner radius is needed to fully avoid problems with hysteresis in such membranes. For physiologically more relevant membranes (such as POPC), the default value of 1.2nm is fine.

## Output frequency
`gmx mdrun` writes some information on the status of the chain coordinate to stdout. The output frequency (in number of steps) is given by
```
export SLICE_COORD_NST_OUTPUT=1000
```
`gmx mdrun` writes the number $`N_p`$ of polar atoms in slice $`s`$, the degree $`\psi(N_p) \in [0,1]`$ to which slice $`s`$ is filled, and the lateral center of mass of polar atoms in slice $`s`$, used to define the lateral position of the cylinder:
```
Slice pulling, step 500, xiChain =   0.193819, pull coord no 0, Zref =    3.66591
----------------------------------------------------
   Slice        N_p  psi(N_p,z)    ComPBCx  ComPBCy
       0     6.5025          1      1.736    2.635
       1     2.4643    0.99691      1.633    1.251
       2    0.97711    0.73283      1.706    1.940
       3          0          0        nan      nan
       4          0          0        nan      nan
...
```
In addition, the x-y-z center of the cylinder and its upper and lower boundary are listed:
```
Cylinder center at (X/Y/Z):             2.06395    1.69676    3.65761
Cylinder spanning z-range (from/to):    2.35761    4.95761
```


## Thickness of slices
We typically used a thickness of 
```
export SLICE_COORD_D=0.1       # Thickness of slices (in nm), default value
```

## Run opening
Now you can run the opening simulation

```
rm -f topol.tpr mdout.mdp
gmx grompp -c 32dmpc.pdb -f pull.mdp -n index.ndx

rm -f pullx.xvg pullf.xvg md.log ener.edr md.lis
rm -f state.cpt state_prev.cpt confout.gro traj_comp.xtc whole.xtc
gmx mdrun -v -s topol.tpr  
```

## Analyze the simulation
Take a look at the development of the reaction coordinate $`\xi_\text{ch}`$:
```
xmgrace pullx.xvg
```
Take a look at the trajectory, e.g. with VMD:
```
echo 0 | gmx trjconv -s -f traj_comp.xtc -o whole.xtc -pbc mol
vmd 32dmpc.pdb whole.xtc
```

The final snapshot should look similar to the file `pore-formed.pdb`
```
pymol pore-formed.pdb
```
<p align="center">
<img width="300" src="Tiny-test-system/pore-formed.jpg">
</p>


# Additional options of the chain coordinate

The following options typically don't need to be changed.

The fraction to which a slice is filled upon the addition of the first atom (parameter $`\zeta`$). No need to change this.
```
export SLICE_COORD_ZETA=0.75
```

You can request that multiple atom must be added to a slice to be consideras as filly by $`\zeta`$. This way you would force
more atoms into the cylinder before reaching $`\xi_\text{ch}`$. For all our applications this was not needed.
```
export SLICE_COORD_GAMMA=1
```
Instead of increasing $`\gamma`$, better reduce the slice thickness, which should have a similar effect.

## Freezing the cylinder at a chosen x-y position

The lateral position of the cylinder is normally dynamically defined to allow the cylinder follow
the defect, as the defect travels laterally in the membrane plane. This is the correct default behaviour and,
in fact, a critical property of the chain coordinate to avoid undesired hysteresis problems.

 If you really want to freeze the cylinder position at a specific x-y position the membrane plane, you can use the environment variables:

```
export SLICE_COORD_FREEZE_CYLINDER=1
export SLICE_COORD_FREEZE_CYLINDER_X=SomeValue
export SLICE_COORD_FREEZE_CYLINDER_Y=SomeOtherValue
```

To freeze the cylinder at its initial position (at time $`t=0`$), use instead
```
export SLICE_COORD_FREEZE_CYLINDER=1
export SLICE_COORD_FREEZE_CYLINDER_X=-1
export SLICE_COORD_FREEZE_CYLINDER_Y=-1
```

 However: If the pore is metastable, freezing the x-y position of the cylinder
 will lead to undesired hysteresis artifacts. Namely, if the cylinder is frozen,
 the system may move down the reaction coordinate by moving the defect *laterally*
 out if the cylinder. In other words: moving along the reaction coordinate
 does not require breaking of the defect. The consequence will be that the PMF
 will probably not resolve the barrier between flat membrane and open pore,
 so the PMF would not reflect any more that the pore is metastable.
 If the pore is not metastable (as e.g. in DOPC), freezing the cylinder will
 probably not have any effect.

## Geometric parameters used for the continuous indicator function
The parameters $`h`$ and $`w`$ explained in the [publication](http://dx.doi.org/10.1021/acs.jctc.7b00106) can be modified with
```
export SLICE_COORD_H=0.25   # default value
export SLICE_COORD_W=0.25   # default value
```
These control the width of the smooth boundaries of the indicator functions, required to render $`\xi_\text{ch}`$ differentiable. No need to change this.

# Common errors

## Cylinder too short
If the cylinder is too short, there will be no polar atom in any of the slices. Then, the lateral x-y position of the cylinder cannot be defied. You will get the error
```
Fatal error:
Weight sum over slices is zero, W = 0.
This happens if all atoms contributing to the chain/slice coordinate are
above and below the cylinder. Probably, you should use more slices
(SLICE_COORD_N).
```
To find a better value for `SLICE_COORD_N`, take an equilibrium simulation (20ns is enough) of a flat, unperturbed membrane. Then do a series of reruns with various SLICE_COORD_N, and choose SLICE_COORD_N such that the average value of pullx.xvg is $`\sim 0.25`$. You can use a script similar to this:
```
n0=5; n1=45                    # smallest and largest trial SLICE_COORD_N
export SLICE_COORD_CYL_R=1.2   # or the Rcyl value you plan to use
rm -f xiAverage-vs-Nslice.txt

for ((n=n0; n<n1; n++)); do
    export SLICE_COORD_N=$n
    echo "Now: SLICE_COORD_N = $SLICE_COORD_N"
    gmx mdrun -rerun traj_compxtc -s rerun.tpr -px rerun_pullx_n$n.xvg -deffnm rerun_tmp_n$n >& md.lis \
        || { cat md.lis; exit 1; }
    # compute average xi[ch] from pullx output:
    xiAverage=$(gmx analyze -f rerun_pullx_n$n.xvg 2> /dev/null | grep ^SS1 | awk '{printf "%f\n", $2}')
    printf "%3d  %12f\n" $n $xiAverage >> xiAverage-vs-Nslice.txt
done
```
And check xiAverage-vs-Nslice.txt

## Simulation instability
Different error messages may indicate simulation instability. Very common are LINCS warning, "Water molecule XX can not be settled", "Box scaled by more than 1%", "An atom moved too far between two domain decomposition steps", or similar. Consider in the following order:
1. Your `pull-coord1-init` is not close to $`\xi_\text{ch}`$ in the first simulation frame, which may cause very large pull forces. Maybe you forgot to set `export SLICE_COORD_N=...`? Check the `pullx.xvg` files. Is the first value close to `pull-coord1-init`? To be sure, do a rerun with your input conformation and check `pullx.xvg` from the rerun.
2. Maybe $`\xi_\text{ch}`$ is not the problem. Try `pull-coord1-k = 0` to turn off all $`\xi_\text{ch}`$-derived forces. If the error still occurs, there is another problem with your simulation, such as atomic clashes. Try a more careful energy minimization.
3. Your force constant might be too large. Try a smaller value, such as `pull-coord1-k = 5000` or `3000`.

